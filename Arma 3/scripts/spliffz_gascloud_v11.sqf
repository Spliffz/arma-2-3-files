/*
	Spliffz Gas Cloud Script v1.1
	
	Makes a gas cloud from which you will get sick and die.
	
	
	We had this nifty feature in ACE, and a friend asked me if it was possible in A3.
	Yes, I said. Thus I present to you this gas cloud script.
	This script allows you to create a deadly gas cloud on a spot.
	Due to the lack of gasmasks, you can protect yourself against it wearing a Jet/Fighter Pilot Helmet.
	Keep in mind that this is still a work in progress.
	I would love to see this be an actual object which can be thrown, but I lack the skills
	to do so.
	
	
	Parameters:
	0: marker - name of the marker on the position where you want the cloud
	
	Returns:
	Well.. what would it be...
	
    
    ["marker"] execVM "spliffz_gascloud_v11.sqf";
    
	
	todo:
	add health depletion in cloud when mask is taken off
	add shaking
	add cloud lifetimer option
	
	� 2013 <thespliffz@gmail.com>
*/

// pre-stuff
spliffz_gas_debug = true;

// this basically gets all the info from the CloudSmallLight cloud config
_sprite_file = getText (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "particleShape");
_sprite_number = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "particleFSNtieth");
_sprite_index = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "particleFSIndex");
_sprite_fcount = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "particleFSFrameCount");
_sprite_type = getText (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "particleType");
_timerPer = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "timerPeriod");
_lifeTime = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "lifeTime");
_position = getArray (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "position");
_moveVel = getArray (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "moveVelocity");
_sim_rotation = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "rotationVelocity");
_sim_weight = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "weight");
_sim_volume = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "volume");
_sim_rubbing = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "rubbing");
_color = getArray (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "color");
_animSpeed = getArray (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "animationSpeed");
_randDirPeriod = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "randomDirectionPeriod");
_randDirIntensity = getNumber (configfile >> "CfgCloudlets" >> "CloudSmallLight" >> "randomDirectionIntensity");

if(spliffz_gas_debug) then {
	// debug
	diag_log format ["SPRITE FILE: %1", _sprite_file ];
	diag_log format ["_sprite_number: %1", _sprite_number ];
	diag_log format ["_sprite_index: %1", _sprite_index ];
	diag_log format ["_sprite_fcount: %1", _sprite_fcount ];
	diag_log format ["_sprite_type: %1", _sprite_type ];
	diag_log format ["_timerPer: %1", _timerPer ];
	diag_log format ["_lifeTime: %1", _lifeTime ];
	diag_log format ["_position: %1", _position ];
	diag_log format ["_moveVel: %1", _moveVel ];
	diag_log format ["_sim_rotation: %1", _sim_rotation ];
	diag_log format ["_sim_weight: %1", _sim_weight ];
	diag_log format ["_sim_volume: %1", _sim_volume ];
	diag_log format ["_sim_rubbing: %1", _sim_rubbing ];
	diag_log format ["_color: %1", _color ];
	diag_log format ["_animSpeed: %1", _animSpeed ];
	diag_log format ["_randDirPeriod: %1", _randDirPeriod ];
	diag_log format ["_randDirIntensity: %1", _randDirIntensity ];
};

// re-set vars to own values if needed (customization!)
_sprite_file = _sprite_file; // "\A3\data_f\ParticleEffects\Universal\Universal_02"
_sprite_number = _sprite_number;
_sprite_index = _sprite_index;
_sprite_fCount = _sprite_fCount;
_sprite_type = "Billboard";
_timerPer = _timerPer;
_lifeTime = _lifeTime;
_position = _position; //[0,0,0];
_moveVel = _moveVel; //[1,1,4.5];
_sim_rotation = _sim_rotation; //0;
_sim_weight = _sim_weight; //10;
_sim_volume = _sim_volume; //2;
_sim_rubbing = _sim_rubbing; //1;
_color = _color; //[[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1]];
_animSpeed = _animSpeed; //[0.125];
_randDirPeriod = _randDirPeriod; //1;
_randDirIntensity = _randDirIntensity; // 0;

_circleSize = 10; // DO NOT CHANGE! Will be customizable in the future! and also not caring about spreadism, yet.
spliffz_gas_protectionGear = ["H_PilotHelmetFighter_B", "H_PilotHelmetFighter_O", "H_PilotHelmetFighter_I"];
spliffz_gas_lifeTime = 60;

if(spliffz_gas_debug) then {
	// another debug for new values
	diag_log format ["SPRITE FILE: %1", _sprite_file ];
	diag_log format ["_sprite_number: %1", _sprite_number ];
	diag_log format ["_sprite_index: %1", _sprite_index ];
	diag_log format ["_sprite_fcount: %1", _sprite_fcount ];
	diag_log format ["_sprite_type: %1", _sprite_type ];
	diag_log format ["_timerPer: %1", _timerPer ];
	diag_log format ["_lifeTime: %1", _lifeTime ];
	diag_log format ["_position: %1", _position ];
	diag_log format ["_moveVel: %1", _moveVel ];
	diag_log format ["_sim_rotation: %1", _sim_rotation ];
	diag_log format ["_sim_weight: %1", _sim_weight ];
	diag_log format ["_sim_volume: %1", _sim_volume ];
	diag_log format ["_sim_rubbing: %1", _sim_rubbing ];
	diag_log format ["_color: %1", _color ];
	diag_log format ["_animSpeed: %1", _animSpeed ];
	diag_log format ["_randDirPeriod: %1", _randDirPeriod ];
	diag_log format ["_randDirIntensity: %1", _randDirIntensity ];
};

//------------------------------------------------------------------------------------------
// No touching/configuring after the next line.
//------------------------------------------------------------------------------------------

spliffz_gas_time = 0;
_gasCloudMarker = _this select 0;

// Functions
spliffz_gas_playerEffects = {
	if((_this select 0) == 1) then {
		// check for protection gear
		if((headgear player) in spliffz_gas_protectionGear) exitWith{};

		// white flashes
		543321 cutText["", "WHITE IN"];
		543321 cutFadeOut (0.2);
		
		// blurry vision aslong as player is in the cloud
		spliffz_fx = ppEffectCreate ["radialBlur", 2005];
		spliffz_fx ppEffectEnable true;
		spliffz_fx ppEffectAdjust [0.2, 0.2, 0.85, 0.85];
		spliffz_fx ppEffectCommit 999;
		spliffz_fx_enabled = true;
		
		while{spliffz_fx_enabled} do {
			543321 cutText["", "WHITE IN"];
			543321 cutFadeOut (0.2);
			sleep (random 4);

			// set damage as long as player is in the cloud
			//_pDamage = (damage player) + (random(0.1));
			//player setDamage _pDamage;
		};
	} else {
		if((count _this) > 1) then {
			if((_this select 0) == 0) then {
				if(!isNil ("spliffz_fx")) then {
					sleep (_this select 1)/4;
/*
					spliffz_fx ppEffectAdjust [0.1, 0.1, 0.9, 0.9];
					spliffz_fx ppEffectCommit 4;
					sleep (_this select 1)/4;
					spliffz_fx ppEffectAdjust [0.05, 0.05, 0.95, 0.95];
					spliffz_fx ppEffectCommit 4;
					sleep (_this select 1)/4;
*/
					spliffz_fx ppEffectEnable false;
					//spliffz_fx = nil;
					spliffz_fx_enabled = false;
				};
			};
		};
	};
};


//------------------------------------------------------------------------------------------


// The gas code.
_pos = getMarkerPos _gasCloudMarker;
_object = "Land_HelipadEmpty_F" createVehicle _pos; // Sign_Sphere10cm_F

// outer-circle
_source1 = "#particlesource" createVehicle _pos;
_source1 setParticleCircle [_circleSize, [0, 0, 0.5]];
_source1 setParticleParams 
					[[_sprite_file, _sprite_number, _sprite_index, _sprite_fcount, 1], "", 
					_sprite_type, 
					_timerPer, 
					_lifeTime, 
					_position, 
					_moveVel, 
					_sim_rotation, _sim_weight, _sim_volume, _sim_rubbing, 
					[4, 12, 20], 
					_color,
					_animSpeed, 
					_randDirPeriod, 
					_randDirIntensity, 
					"", 
					"", 
					_object];
//_source1 setParticleRandom [0, [0.5, 0.5, 0], [0.2, 0.2, 0.2], 0, 0.25, [0, 0, 0, 1], 0, 0];
_source1 setDropInterval 0.1;


// inner-circle
_source2 = "#particlesource" createVehicle _pos;
_source2 setParticleCircle [(_circleSize/2), [0, 0, 0.5]];
_source2 setParticleParams 
					[[_sprite_file, _sprite_number, _sprite_index, _sprite_fcount, 1], "", 
					_sprite_type, 
					_timerPer, 
					_lifeTime, 
					_position, 
					_moveVel, 
					_sim_rotation, _sim_weight, _sim_volume, _sim_rubbing, 
					[4, 12, 20], 
					_color,
					_animSpeed, 
					_randDirPeriod, 
					_randDirIntensity, 
					"", 
					"", 
					_object];

//_source2 setParticleRandom [0, [0.5, 0.5, 0], [0.2, 0.2, 0.2], 0, 0.25, [0, 0, 0, 1], 0, 0];
_source2 setDropInterval 0.1;



// Funky trigger
_trg = createTrigger ["EmptyDetector", _pos];
_trg setTriggerArea [_circleSize, _circleSize, 0, false];
_trg setTriggerActivation ["ANY", "PRESENT", true];
_trg setTriggerStatements ["player in thislist", "[1] spawn spliffz_gas_playerEffects;", "[0,10] spawn spliffz_gas_playerEffects;"];



while {spliffz_gas_time < spliffz_gas_lifeTime} do {
	sleep 1;
};








// EOF