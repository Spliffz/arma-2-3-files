//**************************
// Apache Stunt Script
// v0.1
//
// Made by Spliffz[TDNL] <thespliffz@gmail.com>
//
//**************************


_apache = _this select 0;


_smokeRed = createVehicle ["SmokeShellRed", position _apache, [], 0, "COLLISION"];
_smokeRed attachTo [_apache, [0,0,0]];

_smokeWhite = createVehicle ["SmokeShell", position _apache, [], 0, "COLLISION"];
_smokeWhite attachTo [_apache, [0,0,0]];

_smokeBlue = createVehicle ["SmokeShellBlue", position _apache, [], 0, "COLLISION"];
_smokeBlue attachTo [_apache, [0,0,0]];
