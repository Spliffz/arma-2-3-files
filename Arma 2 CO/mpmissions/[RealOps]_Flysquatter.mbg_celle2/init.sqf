//###################################
//	[RealOps] FlySquatter
//	init.sqf - 2013
//	Spliffz <thespliffz@gmail.com>
//###################################

diag_log format ["[%1] Start", time];

// briefing
//diag_log format ["[%1] Loading Briefing", time];
//execVM "briefing.sqf";

// notes
//diag_log format ["[%1] Loading Notes", time];
//execVM "notes.sqf";

// ### DEBUG
spliffz_debug = true;

diag_log format ["[%1] ####### START SPLIFFZ MISSION #######", time];

titleCut ["", "BLACK FADED", 999];

diag_log format ["[%1] loading defines.sqf", time];
#include "lib\defines.sqf";

diag_log format ["[%1] loading ace.sqf", time];
#include "lib\ace.sqf"; // ace variables etc


if ((!isServer) && (player != player)) then
{
  waitUntil {player == player};
};

leaFunction = compile preprocessFileLineNumbers "lea\loadout-init.sqf"; 
call leaFunction;
call compile preprocessFileLineNumbers "scripts\Init_UPSMON.sqf";	
processInitCommands;


// ACRE AI Listening
execVM "lib\enemy\acre_talk.sqf";

finishMissionInit;


// client stuff after finishMissionInit
execVM "lib\init_client.sqf";


// EOF