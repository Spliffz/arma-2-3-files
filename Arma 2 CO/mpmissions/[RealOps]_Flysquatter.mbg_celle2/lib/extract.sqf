//###################################
//	[RealOps] FlySquatter
//	lib\extract.sqf - 2013
//	Spliffz <thespliffz@gmail.com>
//###################################

private ["_trgSource", "_extractMethod", "_landPos", "_heliSpawn", "_endLZPos", "_heli", "_wp", "_wp2", "_wp3", "_wp4"];


// zet initiele vars, extractMethod dev
_trgSource = _this select 0;
_extractMethod = _this select 1;

// haal waarden van belangrijke locaties op
_landPos = getPos helipad2;
_landPos2 = [_landPos select 0, _landPos select 1, 0];
_heliSpawn = getMarkerPos "extractHeloSpawn";
_endLZPos = getMarkerPos "mrk_end_lz";


// cree�r extraction heli op de spawnplek
_heliGrp = createGroup WEST;
_heli = [_heliSpawn, 0, "UH60M_EP1", _heliGrp] call BIS_fnc_spawnVehicle;
heliGrp = _heliGrp;

// zet wat vars..
_heliVeh = _heli select 0;
_heliCrew = _heli select 1;
_heliGroup = _heli select 2;
_heliVeh setVehicleVarName "helo2";
heliGroup = _heliGroup;
heliCrew = _heliCrew;
heliVeh = _heliVeh;


// trigger for dust-off
trg = createTrigger ["EmptyDetector", getPos player]; 
trg setTriggerArea [0,0,0,false];
trg setTriggerActivation ["ALPHA", "PRESENT", true];
trg setTriggerStatements ["this", "heliVeh doMove (getMarkerPos 'mrk_end_lz')", ""];


//laat player weten dat extract onderweg is
player sidechat format ["Extraction Bird Enroute."];


// geef hem waypoints naar de extract LZ
_heliPos = getPos _heliVeh;
_wp = _heliGrp addWaypoint [_heliPos, 0];
_wp setWaypointBehaviour "AWARE";
_wp setWaypointCombatMode "NO CHANGE";
_wp setWaypointType "MOVE";


// get Low and autohover!
_wp2 = _heliGrp addWaypoint [_landPos, 0];
_wp2 setWaypointBehaviour "AWARE";
_wp2 setWaypointCombatMode "NO CHANGE";
_wp2 setWaypointType "LOAD";
_wp2 setWaypointStatements ["", "(vehicle this) land 'GET IN';"];


// EOF