//###################################
//	[RealOps] FlySquatter
//	lib\defines.sqf - 2013
//	Spliffz <thespliffz@gmail.com>
//###################################

// General script path
fp_path = "lib\";


// Set view distance & environment
maxViewDist = 3000;
setViewDistance maxViewDist;
viewDist = maxViewDist;


// Misc.
player enableIRLasers true;  //client
player enableGunLights true;  //client
player setVariable ["BIS_noCoreConversations", false]; // true
enableSaving [false, false];
 

// EOF