//###################################
//	[RealOps] FlySquatter
//	lib\ace.sqf - 2013
//	Spliffz <thespliffz@gmail.com>
//###################################

// SETUP ACE STUFF
ace_viewdistance_limit = maxViewDist;
ace_sys_dogtag = true;
ace_sys_eject_fnc_weaponCheckEnabled = { false };		publicvariable "ace_sys_eject_fnc_weaponCheckEnabled";

ace_sys_aitalk_enabled = true;							publicVariable "ace_sys_aitalk_enabled";
ace_sys_aitalk_radio_enabled = false;					publicVariable "ace_sys_aitalk_radio_enabled";
ace_sys_aitalk_talkforplayer = false;					publicVariable "ace_sys_aitalk_talkforplayer";


//########## ACE WOUNDING
ace_sys_wounds_enabled = true;							publicVariable "ace_sys_wounds_enabled";
ace_sys_wounds_noai = true;								publicVariable "ace_sys_wounds_noai";
ace_sys_wounds_leftdam = 0;								publicVariable "ace_sys_wounds_leftdam";
ace_sys_wounds_all_medics = false;						publicVariable "ace_sys_wounds_all_medics";
ace_sys_wounds_no_rpunish = true;						publicVariable "ace_sys_wounds_no_rpunish";
ace_sys_wounds_auto_assist_any = true;					publicVariable "ace_sys_wounds_auto_assist_any";
ace_sys_wounds_ai_movement_bloodloss = true;			publicVariable "ace_sys_wounds_ai_movement_bloodloss";
ace_sys_wounds_player_movement_bloodloss = true;		publicVariable "ace_sys_wounds_player_movement_bloodloss";
ace_sys_wounds_auto_assist = true;						publicVariable "ace_sys_wounds_auto_assist";
ace_wounds_prevtime = 300;								publicVariable "ace_wounds_prevtime";
ACE_IFAK_Capacity = 3;
// set revive timer visible
ace_wounds_prevtimeshow = true;                        	publicVariable "ace_wounds_prevtimeshow";
ace_sys_wounds_withSpect = true;						publicVariable "ace_sys_wounds_withSpect";


// EOF