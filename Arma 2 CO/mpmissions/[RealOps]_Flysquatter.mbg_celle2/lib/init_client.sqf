//###################################
//	[RealOps] FlySquatter
//	lib\init_client.sqf - 2013
//	Spliffz <thespliffz@gmail.com>
//###################################

if(isServer && isDedicated) exitWith {};

diag_log format ["[%1] init_client.sqf", time];

private ["_player", "_earplugs", "_welcomeText"];


if !(player hasWeapon "ACE_Earplugs") then { 
	player addWeapon "ACE_Earplugs"; 
}; 
0 fadeRadio 0; 


#define __check configFile >> "CfgIdentities" >> "Identity" >> "name"
_earplugs = {
	if ( ((getText(__check) == "") || (getText(__check) != (name player))) && isMultiplayer ) then { 
		// identity incorrect
		// don't wait
	} else { // wait for init
		waitUntil { 
			sleep 0.5; 
			_earplugs = player getVariable "ace_sys_goggles_earplugs"; 
			!isNil "_earplugs";
		};
	};
	player setVariable ["ace_sys_goggles_earplugs", true, false];
	player setVariable ["ace_ear_protection", true, false];
};
[] spawn _earplugs;



[] Spawn {
	//waitUntil{!(isNil "BIS_fnc_init")};
	_welcomeText = "[RealOps] FlySquatter";
	//titleText [_welcomeText, "PLAIN DOWN"]; 
	//titleFadeOut 8;
	sleep 6;

	// Info text
	[str ("[RealOps] FlySquatter"), str("Celle"), str(date select 1) + "." + str(date select 2) + "." + str(date select 0)] spawn BIS_fnc_infoText;
	titleCut ["", "BLACK IN", 10];
	//sleep 8;
};


// EOF