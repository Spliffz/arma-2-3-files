//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////
//## Medical filler
//if !(isServer) exitWith{};

_timer = 1800;
_amountAmmo = 200;
_amountWeapons = 20;

while {alive _this} do
{
    clearweaponcargo _this;
    clearmagazinecargo _this;
    
    _this addMagazineCargo ["ACE_Bandage", _amountAmmo];
    _this addMagazineCargo ["ACE_Bodybag", _amountAmmo];
    _this addMagazineCargo ["ACE_Epinephrine", _amountAmmo];
    _this addMagazineCargo ["ACE_LargeBandage", _amountAmmo];
    _this addMagazineCargo ["ACE_Medkit", _amountAmmo];
    _this addMagazineCargo ["ACE_Morphine", _amountAmmo];
    _this addMagazineCargo ["ACE_Tourniquet", _amountAmmo];

    sleep _timer;
}; 


// EOF