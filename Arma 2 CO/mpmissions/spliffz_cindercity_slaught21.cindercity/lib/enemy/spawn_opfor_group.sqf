//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////

if !(player == HCSlot) exitwith {}; 

#include "defines.sqf";

_unit = _this select 0;
//_spawnMrk = _this select 1;

//_net = [7,11,24,35,41,53,69,77,85,93];
_net = [1,4,7,11,16,21,24,29,33,35,38,40,41,46,48,51,53,54,59,60,63,65,69,71,74,77,82,85,88,90,93,97];
_ball = round(random 100);	// random choice
_curveBall = round(random 30); // how many groups 

if !(isserver) then {
	if !(player == HCSlot) exitwith {diag_log format ["[CS] %1 Reinforcement Maker exiting on client...",time]};
};
if (isserver) exitwith {diag_log format ["[CS] %1 Exiting Reinforcement Maker",time]};

for [{_n = 1},{_n < _curveBall},{_n = _n+1}] do {
	{
		_ball = round(random 100);
		
		grp_reinf = [getMarkerPos _x, EAST, terroristGroupFull] call BIS_fnc_spawnGroup;
		_unitsInGrp = units grp_reinf;
		{
			_x setSkill ((paramsArray select 2) / 10);
		} forEach _unitsInGrp;
		
		//[grp_reinf, (getPos player)] call BIS_fnc_taskAttack;
		[grp_reinf, getMarkerPos "mrk_opfor_spawn_9", 800] call CBA_fnc_taskAttack;
		//[grp_reinf, getPos player, 800] call CBA_fnc_taskAttack;
		
		sleep 10;
		
		// he shoots
		if (_ball in _net) then {
			// he scores!
			/*
			_distance = 600;
			_dir = random 360;
			_pos = getMarkerPos "mrk_infSpawnCenter";

			_x1 = (_pos select 0) + (_distance * (sin _dir));
			_y1 = (_pos select 1) + (_distance * (cos _dir));

			"mrk_opfor_spawn_9" setMarkerPos [_x1,_y1];
			*/
			
			grp_reinf_Cars = [getMarkerPos _x, EAST, terroristCars1] call BIS_fnc_spawnGroup;
			
			_unitsInGrpCars = units grp_reinf_Cars;
			{
				_x setSkill ((paramsArray select 2) / 10);
			} forEach _unitsInGrpCars;
			
			//[grp_reinf_Cars, (getPos player)] call BIS_fnc_taskAttack;
			[grp_reinf_Cars, getMarkerPos "mrk_opfor_spawn_9", 800] call CBA_fnc_taskAttack;
			//[grp_reinf_Cars, getPos player, 800] call CBA_fnc_taskAttack;
		};
		
		sleep 30;
	} forEach spawnPositionsReinforcements;
};

diag_log["[DEBUG] executing spawn_opfor_group.sqf - Finished"];

// EOF