//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////

#include "defines.sqf";

_unit = _this select 0;
//_spawnMrk = _this select 1;

_net = [7,11,24,33,48,59,60,77,82,93];
_ball = round(random 100);
_curveBall = round(random 30);

if !(isserver) then {
	if !(name player == HCName) exitwith {diag_log format ["[CS] %1 Reinforcement Maker exiting on client...",time]};
};
if (isserver) exitwith {diag_log format ["[CS] %1 Exiting Reinforcement Maker",time]};

if !(isserver) then {
    if (name player == HCName) then {
    
        for [{_n = 1},{_n < _curveBall},{_n = _n+1}] do {
            {
                _ball = round(random 100);
                
                grp_reinf = [getMarkerPos _x, EAST, terroristGroupFull] call BIS_fnc_spawnGroup;
                _unitsInGrp = units grp_reinf;
                {
                    _x setSkill ((paramsArray select 2) / 10);
                } forEach _unitsInGrp;
                
                [grp_reinf, (getPos player)] call BIS_fnc_taskAttack;
                
                // he shoots
                if ( _ball in _net) then {
                    // he scores.. more tango's :D
                    grp_reinf_Cars = [getMarkerPos _x, EAST, terroristCars1] call BIS_fnc_spawnGroup;
                    
                    _unitsInGrpCars = units grp_reinf_Cars;
                    {
                        _x setSkill ((paramsArray select 2) / 10);
                    } forEach _unitsInGrpCars;
                    
                    [grp_reinf_Cars, (getPos player)] call BIS_fnc_taskAttack;
                };
            } forEach spawnPositionsReinforcements;
        };

    };
};
    
// EOF