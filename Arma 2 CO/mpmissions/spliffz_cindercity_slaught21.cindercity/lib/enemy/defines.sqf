//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////

spawnPositions = ["mrk_opfor_spawn_1", "mrk_opfor_spawn_2", "mrk_opfor_spawn_3", "mrk_opfor_spawn_4", "mrk_opfor_spawn_5", "mrk_opfor_spawn_6", "mrk_opfor_spawn_7", "mrk_opfor_spawn_8", "mrk_opfor_spawn_9", "mrk_opfor_spawn_10", "mrk_opfor_spawn_11", "mrk_opfor_spawn_12", "mrk_opfor_spawn_13", "mrk_opfor_spawn_14", "mrk_opfor_spawn_15", "mrk_opfor_spawn_16", "mrk_opfor_spawn_17", "mrk_opfor_spawn_18", "mrk_opfor_spawn_19", "mrk_opfor_spawn_20", "mrk_opfor_spawn_21", "mrk_opfor_spawn_22", "mrk_opfor_spawn_23", "mrk_opfor_spawn_24", "mrk_opfor_spawn_25", "mrk_opfor_spawn_26", "mrk_opfor_spawn_27", "mrk_opfor_spawn_28", "mrk_opfor_spawn_29", "mrk_opfor_spawn_30", "mrk_opfor_spawn_31", "mrk_opfor_spawn_32", "mrk_opfor_spawn_33", "mrk_opfor_spawn_34", "mrk_opfor_spawn_35", "mrk_opfor_spawn_36", "mrk_opfor_spawn_37", "mrk_opfor_spawn_38", "mrk_opfor_spawn_39", "mrk_opfor_spawn_40", "mrk_opfor_spawn_41", "mrk_opfor_spawn_42", "mrk_opfor_spawn_43", "mrk_opfor_spawn_44", "mrk_opfor_spawn_45", "mrk_opfor_spawn_46", "mrk_opfor_spawn_47", "mrk_opfor_spawn_48", "mrk_opfor_spawn_49", "mrk_opfor_spawn_50", "mrk_opfor_spawn_51"];

spawnPositionsGroup1 = ["mrk_opfor_spawn_1", "mrk_opfor_spawn_2", "mrk_opfor_spawn_3", "mrk_opfor_spawn_4", "mrk_opfor_spawn_5", "mrk_opfor_spawn_6", "mrk_opfor_spawn_7", "mrk_opfor_spawn_8", "mrk_opfor_spawn_9", "mrk_opfor_spawn_10", "mrk_opfor_spawn_11", "mrk_opfor_spawn_12", "mrk_opfor_spawn_59"];

spawnPositionGroup2 = ["mrk_opfor_spawn_13", "mrk_opfor_spawn_14", "mrk_opfor_spawn_15", "mrk_opfor_spawn_16", "mrk_opfor_spawn_17", "mrk_opfor_spawn_18", "mrk_opfor_spawn_19", "mrk_opfor_spawn_20", "mrk_opfor_spawn_21", "mrk_opfor_spawn_22", "mrk_opfor_spawn_23", "mrk_opfor_spawn_24"];

spawnPositionGroup3 = ["mrk_opfor_spawn_25", "mrk_opfor_spawn_26", "mrk_opfor_spawn_27", "mrk_opfor_spawn_28", "mrk_opfor_spawn_29", "mrk_opfor_spawn_30", "mrk_opfor_spawn_31", "mrk_opfor_spawn_32", "mrk_opfor_spawn_33", "mrk_opfor_spawn_34", "mrk_opfor_spawn_35", "mrk_opfor_spawn_36"];

spawnPositionGroup4 = ["mrk_opfor_spawn_37", "mrk_opfor_spawn_38", "mrk_opfor_spawn_39", "mrk_opfor_spawn_40", "mrk_opfor_spawn_41", "mrk_opfor_spawn_42", "mrk_opfor_spawn_43", "mrk_opfor_spawn_44", "mrk_opfor_spawn_45", "mrk_opfor_spawn_46", "mrk_opfor_spawn_47", "mrk_opfor_spawn_48", "mrk_opfor_spawn_61"];

spawnPositionGroup5 = ["mrk_opfor_spawn_49", "mrk_opfor_spawn_50", "mrk_opfor_spawn_51", "mrk_opfor_spawn_52", "mrk_opfor_spawn_53", "mrk_opfor_spawn_54", "mrk_opfor_spawn_55", "mrk_opfor_spawn_56", "mrk_opfor_spawn_57", "mrk_opfor_spawn_58", "mrk_opfor_spawn_60", "mrk_opfor_spawn_62", "mrk_opfor_spawn_63", "mrk_opfor_spawn_64"];


spawnPositionsReinforcements = ["mrk_opfor_reinforcements_spawn", "mrk_opfor_reinforcements_spawn_1"];
spawnPositionsReinforcements2 = ["mrk_opfor_reinforcements_spawn_2", "mrk_opfor_reinforcements_spawn_3"];

talibanGroupFull = ["ETaliban1","ETaliban2","ETaliban5","ETaliban6","ETaliban9","ETaliban10","ETaliban3","ETaliban4","ETaliban7","ETaliban8","ETaliban11","ETaliban12"];
talibanGroup1 = ["ETaliban1","ETaliban2","ETaliban5","ETaliban6","ETaliban9","ETaliban10"];
talibanGroup2 = ["ETaliban3","ETaliban4","ETaliban7","ETaliban8","ETaliban11","ETaliban12"];
talibanCars1 =	["EOffroad_SPG9_TERROR","EPickup_PK_TERROR","EOffroad_DSHKM_TERROR"];


terroristGroupFull = ["ETerrorist1","ETerrorist2","ETerrorist3","ETerrorist4","ETerrorist5","ETerrorist6","ETerrorist7","ETerrorist8","ETerrorist9","ETerrorist10","ETerrorist11","ETerrorist12"];
terroristGroup1 = ["ETerrorist1","ETerrorist2","ETerrorist5","ETerrorist6","ETerrorist9","ETerrorist10"];
terroristGroup2 = ["ETerrorist3","ETerrorist4","ETerrorist7","ETerrorist8","ETerrorist11","ETerrorist12"];
terroristCars1 = ["EOffroad_SPG9_TERROR","EPickup_PK_TERROR","EOffroad_DSHKM_TERROR"];

// --------------------------------------------------


// EOF