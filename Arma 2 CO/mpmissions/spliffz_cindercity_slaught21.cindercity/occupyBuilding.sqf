//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////

if !(player == HCSlot) exitwith {}; 

_phase = _this select 0;
_side = "EAST";

diag_log["[DEBUG] executing occupyBuilding.sqf"];

// --------------------------------------------------

#include "lib\enemy\defines.sqf";

// --------------------------------------------------

//hint format["BuildingPos: %1, Building: %2, nObject: %3", _bPos, _building, _nObject];

//_unit setPos ((nearestBuilding _unit) buildingPos _bPos);

// --------------------------------------------------

//_spl_grpLead = talibanGroupFull select (round(random 12));
//diag_log format ["LEAD: %1", _spl_grpLead];

if !(isServer) then {
	if !(player == HCSlot) exitwith {diag_log format ["[CS] %1 Building Populator exiting on client...",time]};
};
if (isServer) exitwith {diag_log format ["[CS] %1 Exiting Building Occupier",time]};

switch (_phase) do {
	case "phase1": {
		// place group on marker
		{   
			_rand = round(random 121);
			//_rand = 38;
			diag_log format["[DEBUG] Random Number #1: %1", _rand];

			if (_rand <= 50) then {
				_mPos = getMarkerPos _x;
				
				grp = [_mPos, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
				
				/*
				// create units
				_grp = createGroup _side;
				_lead = _grp createUnit [_spl_grpLead, _mPos, [], 0, "form"];
				//_lead setVehicleInit _initlstr;
				[_lead] join _grp;
				_grp selectLeader _lead;
				sleep 1;
				
				{
					_newunit = _grp createUnit [_x, _mPos, [], 0, "form"];								
					//_newunit setVehicleInit _initstr;
					[_newunit] join _grp;
				} forEach terroristGroup2;
				*/
				
			};
			if (_rand > 50 && _rand <= 99) then {
				grp = [getMarkerPos _x, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
			};
			if (_rand > 99 && _rand <= 120) then {
				grp = [getMarkerPos _x, EAST, terroristGroupFull] call BIS_fnc_spawnGroup;
			};
			
			sleep 0.1;
			
			_grpLead = leader grp;
			_nBuilding = nearestbuilding _grpLead;
			
			_unitsInGrp = units grp;
			// select all units in group
			{   
				_bPos = round(random 31);
				_x setPos ((nearestBuilding _grpLead) buildingPos _bPos);
				_x setSkill ((paramsArray select 2) / 10);
				
				
				if(_x != _grpLead) then { 
					commandStop _x;
				};
				
				sleep 0.1;
			} forEach _unitsInGrp;
			
			sleep 0.2;
		} forEach spawnPositionsGroup1;
	};
	
	case "phase2": {
		// place group on marker
		{   
			_rand = round(random 121);
			diag_log format["[DEBUG] Random Number #1: %1", _rand];

			if (_rand <= 50) then {
				grp = [getMarkerPos _x, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
			};
			if (_rand > 50 && _rand <= 99) then {
				grp = [getMarkerPos _x, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
			};
			if (_rand > 99 && _rand <= 120) then {
				grp = [getMarkerPos _x, EAST, terroristGroupFull] call BIS_fnc_spawnGroup;
			};
			
			sleep 0.1;
			
			_grpLead = leader grp;
			_nBuilding = nearestbuilding _grpLead;
			
			_unitsInGrp = units grp;
			// select all units in group
			{   
				_bPos = round(random 31);
				_x setPos ((nearestBuilding _grpLead) buildingPos _bPos);
				_x setSkill ((paramsArray select 2) / 10);
				
				
				if(_x != _grpLead) then { 
					commandStop _x;
				};
				
				sleep 0.1;
			} forEach _unitsInGrp;
			
			sleep 0.2;
		} forEach spawnPositionsGroup2;
	};
	
	case "phase3": {
		// place group on marker
		{   
			_rand = round(random 121);
			diag_log format["[DEBUG] Random Number #1: %1", _rand];

			if (_rand <= 50) then {
				grp = [getMarkerPos _x, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
			};
			if (_rand > 50 && _rand <= 99) then {
				grp = [getMarkerPos _x, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
			};
			if (_rand > 99 && _rand <= 120) then {
				grp = [getMarkerPos _x, EAST, terroristGroupFull] call BIS_fnc_spawnGroup;
			};
			
			sleep 0.1;
			
			_grpLead = leader grp;
			_nBuilding = nearestbuilding _grpLead;
			
			_unitsInGrp = units grp;
			// select all units in group
			{   
				_bPos = round(random 31);
				_x setPos ((nearestBuilding _grpLead) buildingPos _bPos);
				_x setSkill ((paramsArray select 2) / 10);
				
				
				if(_x != _grpLead) then { 
					commandStop _x;
				};
				
				sleep 0.1;
			} forEach _unitsInGrp;
			
			sleep 0.2;
		} forEach spawnPositionsGroup3;
	};
	
	case "phase4": {
		// place group on marker
		{   
			_rand = round(random 121);
			diag_log format["[DEBUG] Random Number #1: %1", _rand];

			if (_rand <= 50) then {
				grp = [getMarkerPos _x, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
			};
			if (_rand > 50 && _rand <= 99) then {
				grp = [getMarkerPos _x, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
			};
			if (_rand > 99 && _rand <= 120) then {
				grp = [getMarkerPos _x, EAST, terroristGroupFull] call BIS_fnc_spawnGroup;
			};
			
			sleep 0.1;
			
			_grpLead = leader grp;
			_nBuilding = nearestbuilding _grpLead;
			
			_unitsInGrp = units grp;
			// select all units in group
			{   
				_bPos = round(random 31);
				_x setPos ((nearestBuilding _grpLead) buildingPos _bPos);
				_x setSkill ((paramsArray select 2) / 10);
				
				
				if(_x != _grpLead) then { 
					commandStop _x;
				};
				
				sleep 0.1;
			} forEach _unitsInGrp;
			
			sleep 0.2;
		} forEach spawnPositionsGroup4;
	};
	
	case "phase5": {
		// place group on marker
		{   
			_rand = round(random 121);
			diag_log format["[DEBUG] Random Number #1: %1", _rand];

			if (_rand <= 50) then {
				grp = [getMarkerPos _x, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
			};
			if (_rand > 50 && _rand <= 99) then {
				grp = [getMarkerPos _x, EAST, terroristGroup2] call BIS_fnc_spawnGroup;
			};
			if (_rand > 99 && _rand <= 120) then {
				grp = [getMarkerPos _x, EAST, terroristGroupFull] call BIS_fnc_spawnGroup;
			};
			
			sleep 0.1;
			
			_grpLead = leader grp;
			_nBuilding = nearestbuilding _grpLead;
			
			_unitsInGrp = units grp;
			// select all units in group
			{   
				_bPos = round(random 31);
				_x setPos ((nearestBuilding _grpLead) buildingPos _bPos);
				_x setSkill ((paramsArray select 2) / 10);
				
				
				if(_x != _grpLead) then { 
					commandStop _x;
				};
				
				sleep 0.1;
			} forEach _unitsInGrp;
			
			sleep 0.2;
		} forEach spawnPositionsGroup5;
	};
	
	
};  // END SWITCH


diag_log["[DEBUG] executing occupyBuilding.sqf - Finished"];


// EOF