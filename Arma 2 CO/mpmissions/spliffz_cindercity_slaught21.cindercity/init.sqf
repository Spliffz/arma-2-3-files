//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////

/*
if ((!isServer) && (player != player)) then
{
  waitUntil {player == player};
};
*/

_eastHQ = createCenter east;
grp1 = createGroup east;

_fun = ["phase1"] execVM "occupyBuilding.sqf";

// Crows ! :D
crows = ["mrk_crows",400,100,20] call bis_fnc_crows;

// ACE revive
execVM "revive\ReviveAceWounds.sqf";

// ACRE AI Listening
execVM "lib\enemy\acre_talk.sqf";

// remove NVG's
player removeWeapon "NVGoggles";
//player addWeapon "NVGoggles";
if !(player hasWeapon "ACE_Earplugs") then {player addWeapon "ACE_Earplugs";};

titleText ["Cindercity Slaughter", "BLACK IN", 15];
sleep 15;


// EOF