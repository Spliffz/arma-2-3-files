//////////////////////////////////////////////////
//
//  Mission: CO24 Cindercity Slaughter
//  Headless Client Edition
//  Author: Spliffz[TDNL]
//  Version: 2.4
//
//  December 2012 - Last Update: Januari 2013
//  By Spliffz[TDNL] <thespliffz@gmail.com>
//
//
//
//        A Spliffz Production
//
//
//
//////////////////////////////////////////////////


[INFORMATION]
  Taliban has taken over Cindercity!
  Find them and destroy them. (said in uber heavy voice)
  Inspired by Shacktac's Cindercity mission were they use the headless client.
  As this was not yet publically available at the point I did the initial 
  release, I use a garrison script & DAC to get somewhat same results.
  
  Wait, what? Hell No! Scratch that!
  Rewrote everything: Headless client all the way! :D
  
  Note: 
  Here, have a lolly. Stop whining, start sucking.
  
  
[FEATURES]
  - ACE Wounding + Revive
  - 300+ Terrorists! - To be honest... I don't even want to know.
  - Close Quarter Combat Epicness


[REQUIRED MODS]
  - ACE
  - ACEX
  - ACRE
  - ACR / ACR Lite
  - CBA
  - BB_Mercs
  - DAF (Dutch Armed Forces) & GLT_Missilebox
  - MBG_Cindercity
  - MBG_Killhouses
  - ST_ACRE_VOLUME (just an advise)


//////////////////////////////////////////////////

[CHANGELOG]
   
  [v2.4]
	- AI can now hear you talk
	
   
  [v2.3]
	- Overall Load Balancing*
	- CBA'd it.
	- Overall code fixes & clean up
	- [fixed] Reinforcements Spawn bug
	- [fixed] Headless Client support - this was not optimal due to the name it had to have. It's now universal!
	- [fixed] Ammo crates are now serverside
	- [added] Earplugs to standard gear
	- [added] Crows
	- [added] Garbage Collector
	- [removed] camp2 - 1 big camp now.
	
	*As you might have noticed, 2.0 was a bit buggy and except that, it overloaded the Headless Client on most systems.
	 After a lot of testing, editing and more testing I think I managed to have it all nicely balanced out now.
  
  
  [v2.0]
    - Total Code Revision! No more Garrison, wrote my own stuff.
    - All AI is now controlled by the Headless Client.
    - [removed] Garrison
    - [removed] DAC
    - [added] Headless Client!
    - [added] Set difficulty option
    - [changed] loadscreen
        
  
  [v1.13]
    - [fixed] less cpu stressy :D
    - [added] more enemies! like twice as much :>
    - [added] more players & structure
    - [removed] UPSMon
    - reorganized weapon crates again.
    - overall tweaks & fixes
    - new Loadscreen!

    
  [v1.12]
    - [added] ambience :D
    - [added] Revive Timer parameter
    - [added] Module: Environment effects
    - reorganized weapon crates

    
  [v1.11]
    - Initial release
  
