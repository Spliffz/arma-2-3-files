//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//	Readme - readme.txt
//	
//#######################################

[Intro]
On your way back to base when an RPG hits your chopper. Stranded somewhere in Fallujah you will need to find your way to FOB Holland.


[Outro]
humvees and bradley convoying back to base (FOB Holland, North), transporting you and your squad. Fade-out black > credits.




// TODO
- add suicide bombers & IED's - DONE (maybe in the future change this to vanilla stuff, -@EOD)
- fix intro
- and outro offcourse
- set finish trigger/condition
- make random WEST ammo caches for resupply - DONE
- make spawn random? but that collides with my nice intro...
- add some objectives?
- make it so that you go in the 'deadlike'-unconscious state WITHOUT a life timer! When everybody has this state, game over. - DONE
- add more 'wake-up' animations and randomize them

- add ambience:
	- flying missiles - moker
	- us convoy that tries to pick up, but get's ambushed in front of you - arctor
	- 
	

[FIX]
- spliffz ammo boxes are stuck.. can't get anything out of them (server-sided?)
- game over trigger

	
[CHANGELOG]
[v1.4]
- [removed] UPSMON - bis functions all the way, for now
- [changed] enemies spawn local @ client, instead of server. (thnx Sacha!)
- [added] set difficulty in params


[v1.3]
- [added] fixed game over function, game over when everybody is down (waiting for revive)
- [changed] intro text stuff


[v1.2c]
- [changed] tweaked enemy radio contact
- [changed] tweaked patrol zones
- [changed] end checkpoint is now generated by checkpoints.sqf


[v1.2b]
- [changed] SILVIE init line
- [added] checkpoints
- [added] date parameters
- [added] ammobox @ checkpoints
- [added] patrol @ checkpoints



_newComp = [(getPos this), (getDir this), "CheckPoint1_US_EP1"] call (compile (preprocessFileLineNumbers "ca\modules\dyno\data\scripts\objectMapper.sqf"));

_newComp = [(getPos this), (getDir this), "GuardPost2_US_EP1"] call (compile (preprocessFileLineNumbers "ca\modules\dyno\data\scripts\objectMapper.sqf"));

_newComp = [(getPos this), (getDir this), "GuardPost3_US_EP1"] call (compile (preprocessFileLineNumbers "ca\modules\dyno\data\scripts\objectMapper.sqf"));

["AmovPpneMstpSrasWrflDnon_injuredHealed","AdthPpneMstpSnonWnonDnon_forgoten"]



###############

nul = [21, getMarkerPos "area0", 3, ["area0", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [22, getMarkerPos "area0", 4, ["area0", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [23, getMarkerPos "area0", 2, ["area0", "randomup", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [21, getMarkerPos "area1", 3, ["area1", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [22, getMarkerPos "area1", 4, ["area1", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [23, getMarkerPos "area1", 2, ["area1", "randomup", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [21, getMarkerPos "area2", 3, ["area2", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [22, getMarkerPos "area2", 4, ["area2", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [23, getMarkerPos "area2", 2, ["area2", "randomup", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf";        




nul = [21, getMarkerPos "area4", 2, ["area4", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [22, getMarkerPos "area4", 4, ["area4", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [23, getMarkerPos "area4", 2, ["area4", "randomup", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [21, getMarkerPos "area5", 3, ["area5", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [22, getMarkerPos "area5", 4, ["area5", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [23, getMarkerPos "area5", 2, ["area5", "randomup", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf";        




nul = [21, getMarkerPos "area6", 3, ["area6", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [22, getMarkerPos "area6", 4, ["area6", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [23, getMarkerPos "area6", 2, ["area6", "randomup", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [21, getMarkerPos "area7", 3, ["area7", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [22, getMarkerPos "area7", 4, ["area7", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [23, getMarkerPos "area7", 2, ["area7", "randomup", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [21, getMarkerPos "area8", 2, ["area8", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [22, getMarkerPos "area8", 3, ["area8", "random", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf"; 
nul = [23, getMarkerPos "area8", 2, ["area8", "randomup", "noslow", "nowait", "nofollow", "respawn", "aware", "delete:", 600]] execVM "scripts\UPSMON\MON_spawn.sqf";      


