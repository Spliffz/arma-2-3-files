//#######################################
//
//	Mission: Survive Fallujah 1.3
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Party Trigger Setup - partyTrigger.sqf
//
//#######################################

if(!isServer && !isDedicated) exitWith {};

private ["_pos", "_trg"];

diag_log format ["[%1] Loading Party Trigger", time];

Spliffz_WelcomeParty = compile preProcessFileLineNumbers (fp_path+"welcomeToFallujah.sqf");

_pos = getMarkerPos "mrk_start";

_trg = createTrigger ["EmptyDetector", _pos];
_trg setTriggerArea [50, 50, false];
_trg setTriggerActivation ["WEST", "NOT PRESENT", false];
_trg setTriggerType "NONE";
_trg setTriggerStatements ["this", "null = [] call Spliffz_WelcomeParty; null = [] spawn Spliffz_Spawn_Opfor; deleteVehicle this;", ""];




// EOF