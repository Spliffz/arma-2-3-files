//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Defines - defines.sqf
//
//#######################################

// General script path
fp_path = "lib\";


// Set view distance & environment
maxViewDist = 1000;
setViewDistance maxViewDist;
viewDist = maxViewDist;
envColorIntensity = 1.1;


// Misc.
player enableIRLasers true;  //client
player enableGunLights true;  //client
player setVariable ["BIS_noCoreConversations", false]; // true
enableSaving [false, false];


// Intro stuff
playerMoves_still = ["AinjPpneMstpSnonWrflDnon","AmovPpneMstpSrasWrflDnon_injuredHealed","AdthPpneMstpSnonWnonDnon_forgoten"];
playerMoves_continue = ["aidlppnemstpsraswrfldnon0s"];
welcomeText = ["Aah my head..", "Wtf just happened?!", "Where are we..", "Fuck...", "Am I dead? Is this heaven? Let this be heaven...", "Hell yeah! You missed mothafuckers!", "U-ugh", "Oh bloody hell"];


// Player stuff
sf_players = ["p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8"];
connectedPlayersCount = 0;
connectedPlayers = [];
connPlayers = [];
deadPlayers = [];
Spliffz_unitOut = 0;

 

// EOF