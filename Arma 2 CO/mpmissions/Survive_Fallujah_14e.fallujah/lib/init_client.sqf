//#######################################
//
//	Mission: Survive Fallujah 1.3
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Client Init - init_client.sqf
//
//#######################################

if(isServer && isDedicated) exitWith {};

diag_log format ["[%1] init_client.sqf", time];

private ["_player", "_earplugs", "_welcomeText"];

/*
_player = vehicleVarName player;
diag_log format ["[%1] Setting connectedPlayers + 1: %2", time, _player];
connectedPlayers = connectedPlayers set [count connectedPlayers, _player];
//connectedPlayers = connectedPlayers + [_player];
*/

if !(player hasWeapon "ACE_Earplugs") then { 
	player addWeapon "ACE_Earplugs"; 
}; 
0 fadeRadio 0; 


#define __check configFile >> "CfgIdentities" >> "Identity" >> "name"
_earplugs = {
	if ( ((getText(__check) == "") || (getText(__check) != (name player))) && isMultiplayer ) then { 
		// identity incorrect
		// don't wait
	} else { // wait for init
		waitUntil { 
			sleep 0.5; 
			_earplugs = player getVariable "ace_sys_goggles_earplugs"; 
			!isNil "_earplugs";
		};
	};
	player setVariable ["ace_sys_goggles_earplugs", true, false];
	player setVariable ["ace_ear_protection", true, false];
};
[] spawn _earplugs;


[] Spawn {
	//waitUntil{!(isNil "BIS_fnc_init")};
//	_pMove = playerMoves_still call BIS_fnc_selectRandom;
	player switchMove "AinjPpneMstpSnonWrflDnon";
//	player switchMove _pMove;
	_welcomeText = welcomeText call BIS_fnc_selectRandom;
	titleText [_welcomeText, "PLAIN DOWN"]; 
	titleFadeOut 10;
	sleep 3;

	// Info text
	[str("Somewhere in Fallujah"), str(date select 1) + "." + str(date select 2) + "." + str(date select 0) + " " + str(date select 3) + ":" + str(date select 4)] spawn BIS_fnc_infoText;
	titleCut ["", "BLACK IN", 10];
	sleep 8;
	player switchMove "AinjPpneMstpSnonWrflDnon_rolltofront";  
	sleep 6;
	player switchMove "aidlppnemstpsraswrfldnon0s";  
/*
	_pMove = playerMoves_still call BIS_fnc_selectRandom;
	diag_log format ["_pMove: %1", _pMove];
	player switchMove _pMove;  
	sleep 6;
	_pMove = playerMoves_continue call BIS_fnc_selectRandom;
	player switchMove _pMove;
*/
};


// EOF