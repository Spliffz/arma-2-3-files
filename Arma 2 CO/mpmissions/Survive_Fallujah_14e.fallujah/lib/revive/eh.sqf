//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Revive EventHandlers - revive/eh.sqf
//
//#######################################

//################
// Make it so that when you're unconscious, you don't have a timer and you're not dead.
// make a difference between dead & unconscious for end loop.
// if all players are dead > end. If 1 or more are unconscious, continue till everybody is dead. (there is a difference between 'unconscious' as in deadlike state, and the real 'unconscious' state, from which you 'wake up' after 10-60 seconds. if the latter is the case, then just continue till every player has reached the deadlike state.)


// Need to kill spectator mode on game over!!


// columdrum's terminatespactator thingie - doesn't seem to work though >.< 
Colum_Revive_terminateSpectator={
	if (!ace_sys_spectator_SPECTATINGON) exitwith {};
	
	_timeout =time+4;
	if (isnil "ace_sys_spectator_exit_the_frame") then {
		ace_sys_spectator_exit_the_frame=true;
	};

	if (ace_sys_spectator_exit_the_frame) then {
		//Spectator script initializing wait a bit
		ace_sys_spectator_exit_spectator = nil; //workaround, if you hit respawn when wounded spectator tries to restart but ace_sys_spectator_exit_spectator=true stops it
		waituntil {(_timeout< time) || !ace_sys_spectator_exit_the_frame};
		sleep 0.1;
	};
	//Already started but dialog closed whaaaat why that happens?
	if (!ace_sys_spectator_exit_the_frame && isnull (findDisplay 55001)) then {
		createDialog "ACE_rscSpectate";
	};
	ace_sys_spectator_exit_spectator = true;
	_timeout =time+4;
	waituntil {(_timeout< time) || !ace_sys_spectator_SPECTATINGON};
	if (_timeout< time) then {
		//should never happen with this version
		diag_log["colum_revive spectator close timeout"];
		ace_sys_spectator_exit_spectator = nil;
		if (!ace_sys_spectator_exit_the_frame) then {ace_sys_spectator_exit_the_frame=true};
		_timeout =time+4;
		waituntil {(_timeout< time) || !ace_sys_spectator_SPECTATINGON};
	};
	if (ace_sys_spectator_exit_the_frame && !isnull (findDisplay 55001)) then {	closedialog 0;};
};


private ["_state"];

_state = player getVariable "ace_w_state";
diag_log format ["[%1] Loading revive meuk: ace_w_state: %2", time, _state];


//player setVariable ["ace_sys_spectator_exclude",true,true]; // Changed to public setvar instead of using ace_sys_wounds_uncon

// acre stuff
if (!isnil "acre_api_fnc_setSpectator") then {[true] call acre_api_fnc_setSpectator};

// lock player in spectator
ace_sys_spectator_ShownSides = nil;
ace_sys_spectator_maxDistance = nil;
ace_sys_spectator_CheckDist = nil; 
ace_sys_spectator_CheckUncon = true;
ace_sys_spectator_no_butterfly_mode = true;
ace_sys_spectator_can_exit_spectator = false;
ace_sys_spectator_playable_only = true;					//publicVariable "ace_sys_spectator_playable_only";

//[player, player, ''] spawn ace_fnc_startSpectator;

// kill engine if player == driver == dead
if ((vehicle player!=player) && ((driver(vehicle player)) == player)) then {[] spawn {sleep 10; player action ["engineOff", (vehicle player)]}}; 


// EOF