//#######################################
//
//	Mission: Survive Fallujah 1.3
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Welcome To Fallujah - welcomeToFallujah.sqf
//
//#######################################

//if(isServer && isDedicated) exitWith {};

diag_log format ["[%1] Welcome To Fallujah!", time];


// Let the party begin
//[] call Spliffz_Spawn_Opfor;


welcomeText = {
	["<t size='3.0'>" + "Welcome to FALLUJAH" + "</t>",0,0,5,-1,0,3012] spawn BIS_fnc_dynamicText;
};

[-2, welcomeText] call CBA_fnc_globalExecute;

// EOF