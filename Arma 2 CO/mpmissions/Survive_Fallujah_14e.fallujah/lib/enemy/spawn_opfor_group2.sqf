//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Spawn Opfor Group & Cleanup - spawn_opfor_group2.sqf
//
//#######################################

if(isServer && isDedicated) exitWith {};

private ["_spawnRange", "_maxUnits", "_sleep", "_arrayWithLocalUnits", "_localAICount", "_distanceToPlayer"];

_spawnRange = 500;
_maxUnits = 20;
_sleep = 10;
_arrayWithLocalUnits = [];

if (player == player) then {
    
    while {true} do {
        
        // fetch locals
		{
			if ((local _x) && (side _x == east)) then {
				_arrayWithLocalUnits set [count _arrayWithLocalUnits, _x];
			};
			
		} forEach allUnits;
		_localAICount = count _arrayWithLocalUnits;
		
		
		if(_localAICount > _maxUnits) then {
		
			{
				_distanceToPlayer = player distance _x;
				if(_distanceToPlayer > _spawnRange) then {
					deleteVehicle _x;
				};
				
			} forEach _arrayWithLocalUnits;
			
		};
		
		
		if(_localAICount < _maxUnits) then {
			
			for [{_i = 0}, {_i < (round(random 5))}, {_i = _i + 1}] do {
				_eGrp = createGroup EAST;
				_spawnMarker = [(position player select 0)-_radius*sin(random 359),(position player select 1)-_radius*cos(random 359)];

				_grp = [_spawnMarker, EAST, (configFile >> "CfgGroups" >> "East" >> "BIS_TK" >> "Infantry" >> _enemyGroupsArray select floor (random count _enemyGroupsArray))] call BIS_fnc_spawnGroup;
				_grpLead = leader _grp;
				_unitsInGrp = units _grp;
				
				{
					_x setSkill ((paramsArray select 5) / 10);
				} forEach _unitsInGrp;
				
				sleep 0.2;
				
				
				if(spliffz_debug) then {
					// debug shizzle
					_mrkName = "mrk_"+str(round(random 10000));
					_dbgMrk = createMarker [_mrkName, position _grpLead];
					_dbgMrk setMarkerShape "ICON";
					_dbgMrk setMarkerType "Dot";
					_dbgMrk setMarkerColor "ColorBlue";
					_dbgMrk setMarkerText _mrkName;
				};

				sleep 2;
				
				if(random 1 > 0.5) then {
					[_grp] call Spliffz_Opfor_Search_And_Destroy;
				} else {
					[_grp] call Spliffz_Opfor_Patrol;
				};
				
			};
			
			
		};
		
        sleep _sleep;

    };
	
};


// EOF