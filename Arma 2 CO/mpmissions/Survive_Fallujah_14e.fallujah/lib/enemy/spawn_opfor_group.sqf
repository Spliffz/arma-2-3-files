//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Spawn Opfor Group - spawn_opfor_group.sqf
//
//#######################################
	
if(!isServer && !isDedicated) exitWith {};

private ["_enemyGroupsArray", "_enemyArmorGroupsArray", "_grpPlayer", "_lead", "_grpLead", "_eGrp", "_spawnMarker", "_marker", "_dbgMrk", "_radius", "_mrkName"];


diag_log format ["[%1][OPFOR] Spawning and Commanding groups", time];

_enemyGroupsArray = ["TK_InfantrySection","TK_SniperTeam","TK_MotorizedReconSection"];
_enemyArmorGroups = ["TK_MechanizedInfantrySquadBMP2"];

_marker = _this select 0;
_radius = _this select 1;

{	// foreach _enemyGroupsArray;
	for [{_i = 0}, {_i < (round(random 5))}, {_i = _i + 1}] do {
		_eGrp = createGroup EAST;
		_spawnMarker = [(getMarkerPos _marker select 0)-_radius*sin(random 359),(getMarkerPos _marker select 1)-_radius*cos(random 359)];

		_grp = [_spawnMarker, EAST, (configFile >> "CfgGroups" >> "East" >> "BIS_TK" >> "Infantry" >> _enemyGroupsArray select floor (random count _enemyGroupsArray))] call BIS_fnc_spawnGroup;
		_grpLead = leader _grp;
		 
		 sleep 0.2;
		
		
		if(spliffz_debug) then {
			// debug shizzle
			_mrkName = "mrk_"+str(round(random 10000));
			_dbgMrk = createMarker [_mrkName, position _grpLead];
			_dbgMrk setMarkerShape "ICON";
			_dbgMrk setMarkerType "Dot";
			_dbgMrk setMarkerColor "ColorBlue";
			_dbgMrk setMarkerText _mrkName;
		};

		sleep 2;
		
		if(random 1 > 0.5) then {
			[_grp] call Spliffz_Opfor_Search_And_Destroy;
		} else {
			[_grp] call Spliffz_Opfor_Patrol;
		};
		
	};
	
} forEach _enemyGroupsArray;

// EOF