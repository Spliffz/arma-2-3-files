//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Spawn Opfor - spawn_opfor.sqf
//
//#######################################

if(!isServer && !isDedicated) exitWith { diag_log format ["[%1] spawn_opfor.sqf exiting on client", time]; };

//########################
// spawn group enemies within 300m from player on random pos > attack player or patrol area
// 3x groups, assault squad, recon vics, sniper team
// sniperteam in/on buildings
// delete group after X min when group outside of xxx meter from player


private ["_posGrp", "_marker", "_radius", "_timer", "_randomPlayer"];

diag_log format ["[%1][OPFOR] Executing spawn_opfor.sqf", time];


/*
_grpPlayer = group player;
_lead = leader _grpPlayer;
_posGrp = position _lead;
*/

//diag_log format ["%1 - %2", connectedPlayers, (connectedPlayers select 0)];
// get pos from random player

{
	if((isPlayer _x) && (alive _x)) then {
		connPlayers = connPlayers + [_x];
	};
} forEach playableUnits;

diag_log format ["COUNT CONN PLAYERS: %1", count(connPlayers)];
diag_log format ["CONN PLAYERS: %1", connPlayers select 0];
if(count(connPlayers) > 1) then {
	_randomPlayer = connPlayers select floor(random count connPlayers);
} else {
	_randomPlayer = connPlayers select 0;
};

_posGrp = position _randomPlayer;
_randomID = str(round(random 10000));
_strRandomID = str _randomID;
//_marker = createMarker ["mrk_spawn_opfor", _posGrp];
_marker = createMarker ["mrk_"+_strRandomID, _posGrp];
_marker setMarkerType "Empty";
_timer = 120;
_radius = 500;
diag_log format ["[%1][OPFOR] - playerPos: %2 ", time, _posGrp];


Spliffz_Spawn_Opfor_Group = compile preProcessFileLineNumbers (fp_path+"enemy\spawn_opfor_group2.sqf");
Spliffz_Opfor_Search_And_Destroy = compile preProcessFileLineNumbers (fp_path+"enemy\opfor_search_and_destroy.sqf");
Spliffz_Opfor_Patrol = compile preProcessFileLineNumbers (fp_path+"enemy\opfor_patrol.sqf");
Spliffz_Opfor_Cleanup = compile preProcessFileLineNumbers (fp_path+"enemy\opfor_cleanup.sqf");



while{true} do {

	// spawn_opfor_group
	[_marker, _radius] spawn Spliffz_Spawn_Opfor_Group;
	
	// opfor_cleanup
//	[] call Spliffz_Opfor_Cleanup;

		
	sleep _timer;

};



// EOF