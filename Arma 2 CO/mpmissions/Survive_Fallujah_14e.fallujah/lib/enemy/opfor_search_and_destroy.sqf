//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Opfor Search and Destroy - search_and_destroy.sqf
//
//#######################################

if(!isServer && !isDedicated) exitWith {};

private ["_grp", "_randomPlayer", "_randomPlayerPos", "_ranges", "_randomRange"];

diag_log format ["[%1][OPFOR] Executing Search & Destroy", time];

_grp = _this select 0;

_ranges = [100,200,300,400,500];
_randomRange = _ranges select floor(random count _ranges);

//connPlayers = [];
{
	if((isPlayer _x) && (alive _x)) then {
		connPlayers = connPlayers + [_x];
	};
} forEach playableUnits;


if(count(connPlayers) > 1) then {
	//_randomPlayer = connectedPlayers call BIS_fnc_selectRandom;
	_randomPlayer = connPlayers select floor(random count connPlayers);
} else {
	_randomPlayer = connPlayers select 0;
};

diag_log format ["randomPlayer: %1", _randomPlayer];


//_randomPlayer = connectedPlayers call BIS_fnc_selectRandom;
_randomPlayerPos = position _randomPlayer;

diag_log format ["[%1][OPFOR][S&D] - playerPos: %2 ", time, _randomPlayerPos];


[_grp, _randomPlayerPos] call BIS_fnc_taskAttack;

if(spliffz_debug) then {
	// debug shizzle
	_mrkPos = "mrk_"+str(round(random 10000));
	_dbgMrk = createMarker [_mrkPos, _randomPlayerPos];
	_dbgMrk setMarkerShape "ICON";
	_dbgMrk setMarkerType "Dot";
	_dbgMrk setMarkerColor "ColorRed";
	_dbgMrk setMarkerText _mrkPos;
};

// EOF