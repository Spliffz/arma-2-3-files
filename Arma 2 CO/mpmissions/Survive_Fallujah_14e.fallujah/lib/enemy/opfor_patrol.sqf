//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Opfor Patrol - opfor_patrol.sqf
//
//#######################################

if(!isServer && !isDedicated) exitWith {};

private ["_grp", "_grpLead", "_ranges", "_randomRange"];


diag_log format ["[%1][OPFOR] Executing Patrol Area", time];

_grp = _this select 0;
_grpLead = leader _grp;
_ranges = [100,200,300,400,500];
_randomRange = _ranges select floor(random count _ranges);
_pos = getPos _grpLead;

diag_log format ["[%1][OPFOR][PATROL] - _grpLead Pos: %2 ", time, _pos];


[_grp, _pos, _randomRange] call bis_fnc_taskPatrol;

if(spliffz_debug) then {
	private ["_mrkName", "_dbgMrk"];
	// debug shizzle
	_mrkName = "mrk_"+str(round(random 10000));
	_dbgMrk = createMarker [_mrkName, position _grpLead];
	_dbgMrk setMarkerShape "ICON";
	_dbgMrk setMarkerType "Dot";
	_dbgMrk setMarkerColor "ColorGreen";
	_dbgMrk setMarkerText _mrkName;
};

// EOF