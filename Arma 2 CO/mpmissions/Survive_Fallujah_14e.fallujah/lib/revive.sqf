//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Revive - revive.sqf
//
//#######################################

//################
// Make it so that when you're unconscious, you don't have a timer and you're not dead.
// make a difference between dead & unconscious for end loop.
// if all players are dead > end. If 1 or more are unconscious, continue till everybody is dead. (there is a difference between 'unconscious' as in deadlike state, and the real 'unconscious' state, from which you 'wake up' after 10-60 seconds. if the latter is the case, then just continue till every player has reached the deadlike state.)

/*
_parametros=[player,playerside];
['colum_revive_DeadP', _parametros] call CBA_fnc_globalEvent;

if (Colum_revive_VidasLocal > 0 && (!(player call ace_sys_wounds_fnc_isUncon))) then {
	
};
*/

//Life loss event script
spliffz_sf_revive = compile preProcessFileLineNumbers 'lib\revive\eh.sqf';

// kicks in when player 'dies'
["ace_sys_wounds_rev", {
	player spawn spliffz_sf_revive
}] call CBA_fnc_addEventhandler;


// EOF