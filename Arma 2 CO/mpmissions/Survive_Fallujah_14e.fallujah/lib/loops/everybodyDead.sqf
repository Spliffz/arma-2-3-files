//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  everybody dead Checker - everybodyDead.sqf
//
//#######################################

//if(!isServer && !isDedicated) exitWith {};

//private ["_unitOut"];

Spliffz_Game_Over = {
	diag_log format ["[%1] GAME OVER - units down: %2", time, Spliffz_unitOut];
	sleep 5;
	titleCut ["", "BLACK", 999];
	sleep 3;
	//titleText ["GAME OVER","PLAIN"]; 
	["<t size='2.0'>" + "GAME OVER" + "</t>",0,0,3,-1,0,3011] spawn BIS_fnc_dynamicText;
	//titleFadeOut 3;
	sleep 5;		
};

Spliffz_Game_Over_End_Mission = {
	endMission "END1";
};

while{true} do {

	{
//		if((_x getVariable ["playerSide", side player] == west) && (isPlayer _x)) then {
		if(isPlayer _x) then {
			if((_x getVariable "ace_w_state") == 802) then {
				Spliffz_unitOut = Spliffz_unitOut+1;
				diag_log format ["[%1] units down: %2", time, Spliffz_unitOut];
			};
		};
	} forEach playableUnits;
	
	
	if((Spliffz_unitOut > 0) && (Spliffz_unitOut == connectedPlayersCount)) then {
		
		sleep 3;
		// exit spectator if spectating

		//_spec = player call ace_sys_spectator_spectatingOn
		//if(!(isNil (player call ace_sys_spectator_spectatingOn))) then {
		//if(!(isNil (_spec))) then {


/*  - disabled till fixed. stupid spectatorscreen won't go out.. stays at front.
		if (ace_sys_spectator_SPECTATINGON) then {
			//call Colum_Revive_terminateSpectator;

			//ace_sys_spectator_exit_spectator = true;
			
//			if (isnil "ace_sys_spectator_exit_the_frame") then {
//				ace_sys_spectator_exit_the_frame=true;
//			};
			
//			if (ace_sys_spectator_exit_the_frame && !isnull (findDisplay 55001)) then {	closedialog 0; };

			
			
			titleCut ["", "BLACK", 999];
		};
*/		

		// Game Over
		[-1, {[] call Spliffz_Game_Over}] call CBA_fnc_globalExecute;
		[-2, {[] call Spliffz_Game_Over_End_Mission}] call CBA_fnc_globalExecute;
	};
	
	sleep 10;

};

// EOF