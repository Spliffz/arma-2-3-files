//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Check connected players - connectedPlayers.sqf
//
//#######################################

private ["_player"];

//_player = vehicleVarName player;

while{true} do {

/* 
	{
		if(isPlayer _x) then {
			connectedPlayersCount = connectedPlayersCount+1;
			diag_log format ["[%1] Setting connectedPlayers + 1: %2", time, _player];
			//connectedPlayers = connectedPlayers set [count connectedPlayers, _player];
			connectedPlayers = connectedPlayers + [_player];
		};
	} forEach playableUnits;
*/
	
	{
		if(isPlayer _x) then {
			_player = vehicleVarName _x;
			connectedPlayersCount = connectedPlayersCount+1;
			diag_log format ["[%1] Setting connectedPlayers + 1: %2", time, _player];
			//connectedPlayers = connectedPlayers set [count connectedPlayers, _player];
			connectedPlayers = connectedPlayers + [_player];
		};
	} forEach playableUnits;
	
	sleep 10;
};

// EOF