//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Checkpoint maker - checkpoints.sqf
//
//#######################################

//if(isServer && isDedicated) exitWith {};
if(!isServer && !isDedicated) exitWith {};

private ["_markers", "_checkpoints", "_center", "_logicGroup", "_pos", "_logic", "_cp", "_newComp", "_trg", "_grp", "_grpLead", "_unitsInGrp", "_wp1", "_endCheckpoint", "_vic", "_vehicles", "_i"];

_markers = ["mrk_checkpoint", "mrk_checkpoint_1", "mrk_checkpoint_2", "mrk_checkpoint_3", "mrk_checkpoint_4", "mrk_checkpoint_5", "mrk_checkpoint_6", "mrk_checkpoint_7", "mrk_checkpoint_8", "mrk_checkpoint_9"];
_checkpoints = ["CheckPoint1_US_EP1", "GuardPost_US_EP1", "GuardPost2_US_EP1", "GuardPost3_US_EP1"];
_endCheckpoint = "CheckPoint1_US_EP1";
_vehicles = ["M2A3_EP1", "HMMWV_MK19_DES_EP1", "HMMWV_M1151_M2_DES_EP1"];
_i = 1;

{
	// make checkpoint
	_center = createCenter sideLogic;
	_logicGroup = createGroup _center;
	_pos = getMarkerPos _x;
	_logic = _logicGroup createUnit ["LOGIC", _pos , [], 0, ""];
	_cp = _checkpoints call BIS_fnc_selectRandom;
	_newComp = [(getPos _logic), (getDir _logic), _cp] call (compile (preprocessFileLineNumbers "ca\modules\dyno\data\scripts\objectMapper.sqf"));
	sleep 0.5;

	// make triggers over checkpoint
	_trg = createTrigger ["EmptyDetector", _pos];
	_trg setTriggerArea [50, 50, 0, true];
	_trg setTriggerActivation ["WEST", "PRESENT", true];
	_trg setTriggerStatements ["player in thislist", "hint 'You have entered a US City Checkpoint. Quick, resupply!';", ""];
	_trg setTriggerType "WEST G"; 
	
	// spawn ammobox
	[_x] call spliffz_ammoBox;
	
	/* disabled till I fix it. - not needed anymore due to immortal patrol group
	// lock all vehicles and objects - unlock when player in trg area
	{
		if( _x isKindOf "CAR" || _x typeOf "USBasicWeapons_EP1") then {
			_x lock true;
		}
	} forEach thisList;
	*/
	
	//_centerWest = createCenter WEST;
	// spawn patrol group
	_grp = [_pos, WEST, (configFile >> "CfgGroups" >> "West" >> "BIS_US" >> "Infantry" >> "US_Team")] call BIS_fnc_spawnGroup;
	_grpLead = leader _grp;
	_unitsInGrp = units _grp;
	{
		_x setSkill 1;
		//_x setVehicleInit "this allowDamage false;";
	} forEach _unitsInGrp;
	processInitCommands;
	sleep 0.5;
	
	// and give them something to do
	_wp = _grp addWaypoint [_pos, 0];
	_wp setWaypointBehaviour "AWARE";
	_wp setWaypointCombatMode "NO CHANGE";
	_wp setWaypointType "GUARD";


	if(spliffz_debug) then {
		private ["_mrkName", "_dbgMrk"];
		// debug shizzle
		_mrkName = "mrk_checkpoint_"+str(_i);
		_dbgMrk = createMarker [_mrkName, _pos]; // position _grpLead
		_dbgMrk setMarkerShape "ICON";
		_dbgMrk setMarkerType "Dot";
		_dbgMrk setMarkerColor "ColorYellow";
		_dbgMrk setMarkerText _mrkName;
	};
	_i = _i + 1;
	
	sleep 0.5;
	
} forEach _markers;


// make end checkpoint
_pos = getMarkerPos "trg_end_cp";
_logic = _logicGroup createUnit ["LOGIC", _pos , [], 0, ""];
_cp = _endCheckPoint;
_newComp = [(getPos _logic), (getDir _logic)+90, _cp] call (compile (preprocessFileLineNumbers "ca\modules\dyno\data\scripts\objectMapper.sqf"));

// trigger
_trg = createTrigger ["EmptyDetector", _pos];
_trg setTriggerArea [10, 10, false];
_trg setTriggerActivation ["WEST", "PRESENT", false];
_trg setTriggerStatements ["player in thislist", "hint 'You have reached the US Checkpoint!';", ""];
_trg setTriggerType "WEST G";

// and marker
_mrk = createMarker ["mrk_finish_cp", _pos];
_mrk setMarkerType "Faction_USA_EP1";

// spawn patrol group
_grp = [_pos, WEST, (configFile >> "CfgGroups" >> "West" >> "BIS_US" >> "Infantry" >> "US_Team")] call BIS_fnc_spawnGroup;
_grpLead = leader _grp;

// spawn vehicles
{
	_vic = createVehicle [_x, _pos, [], 30, "FORM"];
	_vic setDir (getDir _logic)+180;
	[_vic] join _grp;
} forEach _vehicles;
sleep 0.5;

_unitsInGrp = units _grp;
{
	_x setSkill 1;
	_x setVehicleInit "this allowDamage false;";
	_x setDir (getDir _logic)+180;
} forEach _unitsInGrp;
processInitCommands;
sleep 0.5;

// and give them something to do
_wp = _grp addWaypoint [_pos, 0];
_wp setWaypointBehaviour "AWARE";
_wp setWaypointCombatMode "NO CHANGE";
_wp setWaypointType "GUARD";

// TODO:
// fix end sequence or something..


// EOF