//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  End Trigger CheckPoint - end_trigger_cp.sqf
//
//#######################################

//if(isServer && isDedicated) exitWith {};
if(!isServer && !isDedicated) exitWith {};

private ["_pos", "_trg", "_mrk"];

_pos = getMarkerPos "trg_end_cp";

_trg = createTrigger ["EmptyDetector", _pos];
_trg setTriggerArea [50, 50, false];
_trg setTriggerActivation ["WEST", "PRESENT", false];
_trg setTriggerStatements ["player in thislist", "hint 'You have reached the US Checkpoint!';", ""];
_trg setTriggerType "WEST G";

_mrk = createMarker ["mrk_finish_cp", _pos];
_mrk setMarkerType "Faction_USA_EP1";

// EOF