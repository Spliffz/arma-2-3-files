//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  set date from parameters - setDate.sqf
//
//#######################################

private ["_month", "_day", "_hour", "_minutes", "_year"];

_month = if(paramsArray select 2 > 0) then { paramsArray select 2; } else { random 12; };
_day = if(paramsArray select 3 > 0) then { paramsArray select 3; } else { random 31; };
_hour = if(paramsArray select 4 > 0) then { paramsArray select 4; } else { random 24; };
_minutes = if(paramsArray select 5 > 0) then { paramsArray select 5; } else { random 60; };
_year = 2013;

setDate [_year, _month, _day, _hour, _minutes];

// EOF