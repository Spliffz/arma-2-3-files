//#######################################
//
//	Mission: Survive Fallujah 1.3
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Disable Respawn Button - noMoreRespawn.sqf
//
//#######################################

private["_display","_btnRespawn"];

//while{true} do {

	disableSerialization;
	waitUntil {
		_display = findDisplay 49;
		!isNull _display;
	};
	_btnRespawn = _display displayCtrl 1010;
	_btnRespawn ctrlEnable false;

//};

// EOF