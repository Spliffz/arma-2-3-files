//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  ACE Variables - ace.sqf
//
//#######################################

// SETUP ACE STUFF
ace_viewdistance_limit = maxViewDist;  // 1000
ace_sys_dogtag = true;
ace_sys_eject_fnc_weaponCheckEnabled = { false };		publicvariable "ace_sys_eject_fnc_weaponCheckEnabled";

ace_sys_aitalk_enabled = true;							publicVariable "ace_sys_aitalk_enabled";
ace_sys_aitalk_radio_enabled = false;					publicVariable "ace_sys_aitalk_radio_enabled";
ace_sys_aitalk_talkforplayer = false;					publicVariable "ace_sys_aitalk_talkforplayer";

//########## ACE WOUNDING
ace_sys_wounds_enabled = true;							publicVariable "ace_sys_wounds_enabled";
ace_sys_wounds_noai = true;								publicVariable "ace_sys_wounds_noai";
ace_sys_wounds_leftdam = 0;								publicVariable "ace_sys_wounds_leftdam";
ace_sys_wounds_all_medics = true;						publicVariable "ace_sys_wounds_all_medics";
ace_sys_wounds_no_rpunish = true;						publicVariable "ace_sys_wounds_no_rpunish";
ace_sys_wounds_auto_assist_any = true;					publicVariable "ace_sys_wounds_auto_assist_any";
ace_sys_wounds_ai_movement_bloodloss = true;			publicVariable "ace_sys_wounds_ai_movement_bloodloss";
ace_sys_wounds_player_movement_bloodloss = true;		publicVariable "ace_sys_wounds_player_movement_bloodloss";
ace_sys_wounds_auto_assist = true;						publicVariable "ace_sys_wounds_auto_assist";
//ace_wounds_prevtime = paramsArray select 0;				publicVariable "ace_wounds_prevtime";
ace_wounds_prevtime = 9999;								publicVariable "ace_wounds_prevtime";
ACE_IFAK_Capacity = 3;
// set revive timer visible
ace_wounds_prevtimeshow = true;                        publicVariable "ace_wounds_prevtimeshow";
ace_sys_wounds_withSpect = true;						publicVariable "ace_sys_wounds_withSpect";


// EOF