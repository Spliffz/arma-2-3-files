//#######################################
//
//	Mission: Survive Fallujah 1.3
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Gear Players ACE VERSION - gearPlayers.sqf 
//
//#######################################

if(isServer && isDedicated) exitWith {};

private ["_unit"];


ace_sys_wounds_no_medical_gear = true;

_unit = vehicleVarName player;

//diag_log format ["[%1] _unit: %2",time, _unit];

switch (_unit) do {
	case "p1": {
			removeallweapons p1;
			removeallitems p1;
			removeBackpack p1;

			[p1,1,1,0,true] call ACE_fnc_PackIFAK;
			_p1playerweap = [
				"RH_M4CGLACOG",
				"ACE_CharliePack",
				"ACE_USPSD",
				"NVGoggles",
				"Binocular_Vector",
				"ACRE_PRC152",
				"ACE_GlassesBalaklava",
				"ItemCompass",
				"ACE_DAGR",
				"ACE_KeyCuffs",
				"ACE_Earplugs",
				"ACE_Map",
				"ACE_Map_Tools",
				"ACE_GlassesBlackSun",
				"ItemWatch"
			];

			_p1playerammo = [
				"HandGrenade_West",
				"HandGrenade_West",
				"SmokeShell",
				"SmokeShell",
				"ACE_Battery_Rangefinder",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"IR_Strobe_Marker",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203"
			];

			{p1 addmagazine _x} foreach _p1playerammo;
			{p1 addWeapon _x} foreach _p1playerweap;

			[p1, "ACE_Bandage", 4] call ACE_fnc_PackMagazine;
			[p1, "ACE_Epinephrine", 2] call ACE_fnc_PackMagazine;
			[p1, "ACE_LargeBandage", 4] call ACE_fnc_PackMagazine;
			[p1, "ACE_Medkit", 2] call ACE_fnc_PackMagazine;
			[p1, "ACE_Morphine", 2] call ACE_fnc_PackMagazine;
			[p1, "ACE_30Rnd_556x45_T_Stanag", 5] call ACE_fnc_PackMagazine;
			[p1, "ACE_1Rnd_HE_M203", 9] call ACE_fnc_PackMagazine;
			[p1, "FlareWhite_M203", 2] call ACE_fnc_PackMagazine;
			[p1, "FlareRed_M203", 1] call ACE_fnc_PackMagazine;
			[p1, "PipeBomb", 1] call ACE_fnc_PackMagazine;
			[p1, "ACE_Knicklicht_IR", 1] call ACE_fnc_PackMagazine;
			[p1, "ACE_M72"] call ACE_fnc_PutWeaponOnBack;

			p1 selectWeapon (primaryWeapon p1);

		};
	
	case "p2": {
			removeallweapons p2;
			removeallitems p2;
			removeBackpack p2;

			[p2,1,1,0,true] call ACE_fnc_PackIFAK;
			_p2playerweap = [
				"RH_M4CAIM",
				"MAAWS",
				"ACE_USPSD",
				"NVGoggles",
				"Binocular_Vector",
				"ACRE_PRC152",
				"ACE_GlassesBalaklava",
				"ItemCompass",
				"ACE_DAGR",
				"ACE_KeyCuffs",
				"ACE_Earplugs",
				"ACE_Map",
				"ACE_Map_Tools",
				"ACE_GlassesBlackSun",
				"ItemWatch"
			];

			_p2playerammo = [
				"HandGrenade_West",
				"HandGrenade_West",
				"SmokeShell",
				"SmokeShell",
				"ACE_Battery_Rangefinder",
				"ACE_30Rnd_556x45_T_Stanag",
				"MAAWS_HEDP",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_Knicklicht_B"
			];

			{p2 addmagazine _x} foreach _p2playerammo;
			{p2 addWeapon _x} foreach _p2playerweap;

			[p2, "ACE_Epinephrine", 2] call ACE_fnc_PackMagazine;
			[p2, "ACE_Bandage", 4] call ACE_fnc_PackMagazine;
			[p2, "ACE_LargeBandage", 4] call ACE_fnc_PackMagazine;
			[p2, "ACE_Morphine", 2] call ACE_fnc_PackMagazine;
			[p2, "ACE_Medkit", 2] call ACE_fnc_PackMagazine;
			[p2, "MAAWS_HEDP", 2] call ACE_fnc_PackMagazine;
			[p2, "ACE_30Rnd_556x45_T_Stanag", 4] call ACE_fnc_PackMagazine;
			[p2, "ACE_Knicklicht_IR", 1] call ACE_fnc_PackMagazine;
			[p2, "ACE_CharliePack"] call ACE_fnc_PutWeaponOnBack;

			p2 selectWeapon (primaryWeapon p2);
		};
	
	case "p3": {

			removeallweapons p3;
			removeallitems p3;
			removeBackpack p3;

			[p3,1,1,0,true] call ACE_fnc_PackIFAK;
				_p3playerweap = [
				"RH_M4CAIM",
				"ACE_Rucksack_MOLLE_WMARPAT_Medic",
				"ACE_USPSD",
				"NVGoggles",
				"Binocular_Vector",
				"ACRE_PRC152",
				"ACE_GlassesBalaklava",
				"ItemCompass",
				"ACE_DAGR",
				"ACE_KeyCuffs",
				"ACE_Earplugs",
				"ACE_Map",
				"ACE_Map_Tools",
				"ACE_GlassesBlackSun",
				"ItemWatch"
			];

			_p3playerammo = [
				"HandGrenade_West",
				"HandGrenade_West",
				"SmokeShell",
				"SmokeShell",
				"ACE_Battery_Rangefinder",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"SmokeShell",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD"
			];

			{p3 addmagazine _x} foreach _p3playerammo;
			{p3 addWeapon _x} foreach _p3playerweap;

			[p3, "ACE_Bandage", 13] call ACE_fnc_PackMagazine;
			[p3, "ACE_Epinephrine", 11] call ACE_fnc_PackMagazine;
			[p3, "ACE_Medkit", 9] call ACE_fnc_PackMagazine;
			[p3, "ACE_Morphine", 11] call ACE_fnc_PackMagazine;
			[p3, "ACE_LargeBandage", 19] call ACE_fnc_PackMagazine;
			[p3, "ACE_30Rnd_556x45_T_Stanag", 6] call ACE_fnc_PackMagazine;
			[p3, "SmokeShell", 3] call ACE_fnc_PackMagazine;
			[p3, "ACE_Knicklicht_IR", 1] call ACE_fnc_PackMagazine;
			[p3, ""] call ACE_fnc_PutWeaponOnBack;

			p3 selectWeapon (primaryWeapon p3);
		
		};
	
	case "p4": {
			removeallweapons p4;
			removeallitems p4;
			removeBackpack p4;

			[p4,1,1,0,true] call ACE_fnc_PackIFAK;
			_p4playerweap = [
				"ACE_M14_ACOG",
				"ACE_CharliePack",
				"ACE_USPSD",
				"NVGoggles",
				"Binocular_Vector",
				"ACRE_PRC148_UHF",
				"ACE_GlassesBalaklava",
				"ItemCompass",
				"ACE_DAGR",
				"ACE_KeyCuffs",
				"ACE_Earplugs",
				"ACE_Map",
				"ACE_Map_Tools",
				"ACE_GlassesBlackSun",
				"ACE_GlassesTactical",
				"ItemWatch"
			];

			_p4playerammo = [
				"HandGrenade_West",
				"HandGrenade_West",
				"SmokeShell",
				"SmokeShell",
				"ACE_Battery_Rangefinder",
				"IR_Strobe_Marker",
				"ACE_20Rnd_762x51_T_DMR",
				"ACE_20Rnd_762x51_T_DMR",
				"ACE_20Rnd_762x51_T_DMR",
				"ACE_20Rnd_762x51_T_DMR",
				"ACE_20Rnd_762x51_T_DMR",
				"ACE_20Rnd_762x51_T_DMR",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD"
			];

			{p4 addmagazine _x} foreach _p4playerammo;
			{p4 addWeapon _x} foreach _p4playerweap;

			[p4, "ACE_Bandage", 2] call ACE_fnc_PackMagazine;
			[p4, "ACE_Epinephrine", 2] call ACE_fnc_PackMagazine;
			[p4, "ACE_LargeBandage", 2] call ACE_fnc_PackMagazine;
			[p4, "ACE_Medkit", 2] call ACE_fnc_PackMagazine;
			[p4, "ACE_Morphine", 2] call ACE_fnc_PackMagazine;
			[p4, "ACE_20Rnd_762x51_T_DMR", 11] call ACE_fnc_PackMagazine;
			[p4, "SmokeShell", 3] call ACE_fnc_PackMagazine;
			[p4, "PipeBomb", 1] call ACE_fnc_PackMagazine;
			[p4, "ACE_Knicklicht_IR", 1] call ACE_fnc_PackMagazine;
			[p4, ""] call ACE_fnc_PutWeaponOnBack;

			p4 selectWeapon (primaryWeapon p4);

		};
	
	case "p5": {
			removeallweapons p5;
			removeallitems p5;
			removeBackpack p5;

			[p5,1,1,0,true] call ACE_fnc_PackIFAK;
				_p5playerweap = [
				"M249_m145_EP1",
				"ACE_CharliePack",
				"ACE_USPSD",
				"NVGoggles",
				"ACRE_PRC148_UHF",
				"ItemCompass",
				"ACE_DAGR",
				"ACE_Earplugs",
				"ItemGPS",
				"ACE_Map",
				"ACE_Map_Tools",
				"ItemWatch"
			];

			_p5playerammo = [
				"SmokeShell",
				"SmokeShell",
				"HandGrenade_West",
				"HandGrenade_West",
				"IR_Strobe_Marker",
				"ACE_200Rnd_556x45_T_M249",
				"ACE_200Rnd_556x45_T_M249",
				"ACE_200Rnd_556x45_T_M249",
				"HandGrenade_West",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD"
			];

			{p5 addmagazine _x} foreach _p5playerammo;
			{p5 addWeapon _x} foreach _p5playerweap;

			[p5, "ACE_LargeBandage", 6] call ACE_fnc_PackMagazine;
			[p5, "ACE_Medkit", 2] call ACE_fnc_PackMagazine;
			[p5, "ACE_200Rnd_556x45_T_M249", 3] call ACE_fnc_PackMagazine;
			[p5, "ACE_Knicklicht_IR", 1] call ACE_fnc_PackMagazine;
			[p5, ""] call ACE_fnc_PutWeaponOnBack;

			p5 selectWeapon (primaryWeapon p5);

		};
	
	case "p6": {
			removeallweapons p6;
			removeallitems p6;
			removeBackpack p6;

			[p6,1,1,0,true] call ACE_fnc_PackIFAK;
			_p6playerweap = [
				"RH_M4CGLACOG",
				"ACE_CharliePack",
				"ACE_USPSD",
				"NVGoggles",
				"Binocular_Vector",
				"ACRE_PRC152",
				"ACE_GlassesBalaklava",
				"ItemCompass",
				"ACE_DAGR",
				"ACE_KeyCuffs",
				"ACE_Earplugs",
				"ACE_Map",
				"ACE_Map_Tools",
				"ACE_GlassesBlackSun",
				"ItemWatch"
			];

			_p6playerammo = [
				"HandGrenade_West",
				"HandGrenade_West",
				"SmokeShell",
				"SmokeShell",
				"ACE_Battery_Rangefinder",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"IR_Strobe_Marker",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203"
			];

			{p6 addmagazine _x} foreach _p6playerammo;
			{p6 addWeapon _x} foreach _p6playerweap;

			[p6, "RH_M4CGLACOG", 1] call ACE_fnc_PackWeapon;
			[p6, "ACE_Epinephrine", 2] call ACE_fnc_PackMagazine;
			[p6, "ACE_Bandage", 3] call ACE_fnc_PackMagazine;
			[p6, "ACE_LargeBandage", 1] call ACE_fnc_PackMagazine;
			[p6, "ACE_Morphine", 2] call ACE_fnc_PackMagazine;
			[p6, "ACE_Medkit", 2] call ACE_fnc_PackMagazine;
			[p6, "ACE_1Rnd_HE_M203", 12] call ACE_fnc_PackMagazine;
			[p6, "ACE_30Rnd_556x45_T_Stanag", 5] call ACE_fnc_PackMagazine;
			[p6, "FlareWhite_M203", 3] call ACE_fnc_PackMagazine;
			[p6, "1Rnd_SmokeGreen_M203", 3] call ACE_fnc_PackMagazine;
			[p6, "1Rnd_SmokeRed_M203", 2] call ACE_fnc_PackMagazine;
			[p6, "ACE_Knicklicht_IR", 1] call ACE_fnc_PackMagazine;
			[p6, "ACE_M72A2"] call ACE_fnc_PutWeaponOnBack;

			p6 selectWeapon (primaryWeapon p6);
		
		};
	
	case "p7": {
			removeallweapons p7;
			removeallitems p7;
			removeBackpack p7;

			[p7,1,1,0,true] call ACE_fnc_PackIFAK;
			_p7playerweap = [
				"RH_M4CACOG",
				"ACE_CharliePack",
				"ACE_USPSD",
				"NVGoggles",
				"Binocular_Vector",
				"ACRE_PRC152",
				"ACE_GlassesBalaklava",
				"ItemCompass",
				"ACE_DAGR",
				"ACE_KeyCuffs",
				"ACE_Earplugs",
				"ACE_Map",
				"ACE_Map_Tools",
				"ACE_GlassesBlackSun",
				"ItemWatch"
			];

			_p7playerammo = [
				"HandGrenade_West",
				"HandGrenade_West",
				"SmokeShell",
				"SmokeShell",
				"ACE_Battery_Rangefinder",
				"IR_Strobe_Marker",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_30Rnd_556x45_T_Stanag",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203",
				"ACE_1Rnd_HE_M203"
			];

			{p7 addmagazine _x} foreach _p7playerammo;
			{p7 addWeapon _x} foreach _p7playerweap;

			[p7, "ACE_Epinephrine", 2] call ACE_fnc_PackMagazine;
			[p7, "ACE_Bandage", 3] call ACE_fnc_PackMagazine;
			[p7, "ACE_LargeBandage", 1] call ACE_fnc_PackMagazine;
			[p7, "ACE_Morphine", 1] call ACE_fnc_PackMagazine;
			[p7, "ACE_Medkit", 2] call ACE_fnc_PackMagazine;
			[p7, "ACE_30Rnd_556x45_T_Stanag", 14] call ACE_fnc_PackMagazine;
			[p7, "MAAWS_HEDP", 1] call ACE_fnc_PackMagazine;
			[p7, "ACE_200Rnd_556x45_T_M249", 1] call ACE_fnc_PackMagazine;
			[p7, "ACE_Knicklicht_IR", 1] call ACE_fnc_PackMagazine;
			[p7, "ACE_M72A2"] call ACE_fnc_PutWeaponOnBack;

			p7 selectWeapon (primaryWeapon p7);

		};
		
	case "p8": {
			removeallweapons p8;
			removeallitems p8;
			removeBackpack p8;

			[p8,1,1,0,true] call ACE_fnc_PackIFAK;
			_p8playerweap = [
				"M249_m145_EP1",
				"ACE_CharliePack",
				"ACE_USPSD",
				"NVGoggles",
				"ACRE_PRC148_UHF",
				"ItemCompass",
				"ACE_DAGR",
				"ACE_Earplugs",
				"ItemGPS",
				"ACE_Map",
				"ACE_Map_Tools",
				"ItemWatch"
			];

			_p8playerammo = [
				"SmokeShell",
				"SmokeShell",
				"HandGrenade_West",
				"HandGrenade_West",
				"IR_Strobe_Marker",
				"ACE_200Rnd_556x45_T_M249",
				"ACE_200Rnd_556x45_T_M249",
				"ACE_200Rnd_556x45_T_M249",
				"HandGrenade_West",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD",
				"ACE_12Rnd_45ACP_USPSD"
			];

			{p8 addmagazine _x} foreach _p8playerammo;
			{p8 addWeapon _x} foreach _p8playerweap;

			[p8, "ACE_LargeBandage", 6] call ACE_fnc_PackMagazine;
			[p8, "ACE_Medkit", 2] call ACE_fnc_PackMagazine;
			[p8, "ACE_200Rnd_556x45_T_M249", 3] call ACE_fnc_PackMagazine;
			[p8, "ACE_Knicklicht_IR", 1] call ACE_fnc_PackMagazine;
			[p8, ""] call ACE_fnc_PutWeaponOnBack;

			p8 selectWeapon (primaryWeapon p8);
		
		};
		
	// END SWITCH
};



// EOF