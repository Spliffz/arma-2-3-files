//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Ammobox creator - cp_ammobox.sqf
//
//#######################################

//if(isServer && isDedicated) exitWith {};
if(isDedicated) exitWith {};

private ["_box", "_amountAmmo", "_amountWeapons", "_marker", "_mags", "_weapons"];

_marker = _this select 0;
_amountAmmo = 100;
_amountWeapons = 5;

_mags = ["ACE_30Rnd_556x45_SB_S_Stanag","ACE_30Rnd_556x45_T_Stanag","8Rnd_B_Beneli_74Slug","8Rnd_B_Beneli_Pellets","17Rnd_9x19_glock17","ACE_12Rnd_45ACP_USP","ACE_12Rnd_45ACP_USPSD","ACE_15Rnd_9x19_S_M9","ACE_FlareIR_M203","ACE_SSWhite_M203","ACE_SSYellow_M203","ACE_SSGreen_M203","ACE_SSRed_M203","SmokeShell","SmokeShellBlue","SmokeShellGreen","SmokeShellOrange","SmokeShellPurple","SmokeShellRed","SmokeShellYellow","ACE_100Rnd_556x45_T_M249","ACE_200Rnd_556x45_T_M249","100Rnd_762x51_M240","HandGrenade_West","ACE_1Rnd_HE_M203","30Rnd_556x45_G36","ACE_20Rnd_762x51_T_HK417","ACE_Bandage","ACE_Epinephrine","ACE_LargeBandage","ACE_Medkit","ACE_Morphine"];

_weapons = ["RH_m4c","M16A2","M16A2GL","M16A4_GL","M4A1","ACE_M4_GL","ACE_M4_C","ACE_M16A4_Iron","ACE_M4A1_GL","ACE_SOC_M4A1","ACE_SOC_M4A1_GL","ACE_SOC_M4A1_GL_13","ACE_HK416_D14","ACE_HK416_D10_M320","ACE_G36K_iron_D","ACE_M27_IAR","M240","M249_EP1","M249"];

//_box = "USBasicWeapons_EP1" createVehicle (getMarkerPos _marker);
_box = createVehicle ["USBasicWeapons_EP1", [(getMarkerPos _marker select 0)+2, (getMarkerPos _marker select 1)+4, 0], [], 0, "CAN_COLLIDE"];


clearWeaponCargo _box;
clearMagazineCargo _box;

{
	_box addMagazineCargo [_x, random _amountAmmo];
} forEach _mags;

{
	_box addWeaponCargo [_x, random _amountWeapons];
} forEach _weapons;
		
		
if(spliffz_debug) then {
	private ["_mrkName", "_dbgMrk"];
	// debug shizzle
	_mrkName = "mrk_ammo_"+str(round(random 10000));
	_dbgMrk = createMarker [_mrkName, getMarkerPos _marker];
	_dbgMrk setMarkerShape "ICON";
	_dbgMrk setMarkerType "Dot";
	_dbgMrk setMarkerColor "ColorBlack";
	_dbgMrk setMarkerText _mrkName;
};

// EOF