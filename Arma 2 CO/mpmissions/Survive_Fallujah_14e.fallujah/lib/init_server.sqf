//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Server init - init_server.sqf
//
//#######################################

//if(!isServer) exitWith {};
if(!isServer && !isDedicated) exitWith {};

// Garbage Collector
if (isnil "BIS_GC_trashItFunc") then {
	BIS_GC_trashItFunc = (createGroup sideLogic) createUnit ["GarbageCollector", [0,0,0], [], 0, "NONE"];
};


// Environment - Colors
if (isnil "WeatherPostprocessManager") then {
	WeatherPostprocessManager = (createGroup sideLogic) createUnit ["WeatherPostprocessManager", [0,0,0], [], 0, "NONE"];
	WeatherPostprocessManager setVariable ["intensity", envColorIntensity];
};


// SETUP ACRE SYNC IF ACRE IS LOADED
if (isClass(configFile >> "CfgPatches" >> "acre_main")) then {
	acre_sys_core_sync_enabled = true;
};


// Ambient Civies
if (isnil "Alice2Manager") then {
	sleep 90;
	diag_log format ["[%1] loading ALICE", time];
	Alice = (createGroup sideLogic) createUnit ["Alice2Manager", [0,0,0], [], 0, "NONE"];
	Alice setVariable ["ALICE_civillianinit", [{
							call compile preProcessFileLineNumbers "mip\init-alice2.sqf";
						}]
					];
};


// Ambient Vehicles (Silvie)
if (isnil "SilvieManager") then {
	Silvie = (createGroup sideLogic) createUnit ["SilvieManager", [0,0,0], [], 0, "NONE"];
	Silvie setVariable ["SILVIE_vehicleinit", [{
							call compile preProcessFileLineNumbers "mip\init-silvie.sqf";
						}]
					];
};


// EOF