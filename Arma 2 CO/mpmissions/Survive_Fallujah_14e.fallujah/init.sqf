//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  init.sqf
//
//#######################################

diag_log format ["[%1] Start", time];

// briefing
diag_log format ["[%1] Loading Briefing", time];
execVM "briefing.sqf";

// notes
//diag_log format ["[%1] Loading Notes", time];
//execVM "notes.sqf";

// ### DEBUG
spliffz_debug = true;

diag_log format ["[%1] ####### START SPLIFFZ MISSION #######", time];

titleCut ["", "BLACK FADED", 999];

//call compile preProcessFileLineNumbers "scripts\Init_UPSMON.sqf";	

diag_log format ["[%1] loading defines.sqf", time];
#include "lib\defines.sqf";

diag_log format ["[%1] loading server init", time];
execVM "lib\init_server.sqf";

diag_log format ["[%1] loading ace.sqf", time];
#include "lib\ace.sqf"; // ace variables etc

diag_log format ["[%1] loading revive.sqf", time];
execVM "lib\revive.sqf"; // revive/wounds meuk

diag_log format ["[%1] loading ammobox.sqf", time];
spliffz_ammoBox = compile preProcessFileLineNumbers (fp_path+"ammo\cp_ammobox.sqf");

diag_log format ["[%1] setting date", time];
// set Date from params
_null = call compile preProcessFileLineNumbers (fp_path+"func\setDate.sqf");

diag_log format ["[%1] placing checkpoints", time];
// place stuff
_null = call compile preProcessFileLineNumbers (fp_path+"func\checkpoints.sqf");

//diag_log format ["[%1] placing end checkpoint", time]; - disabled, done with checkpoints
//_null = call compile preProcessFileLineNumbers (fp_path+"func\end_trigger_cp.sqf");

private ["_player"];
_player = vehicleVarName player;
diag_log format ["[%1] Setting connectedPlayers + 1: %2", time, _player];
//connectedPlayers = connectedPlayers set [count connectedPlayers, _player];
connectedPlayers = connectedPlayers + [_player];


// loading opfor 
Spliffz_Spawn_Opfor = compile preProcessFileLineNumbers (fp_path+"enemy\spawn_opfor.sqf");
 

// Bye Bye respawnbutton - Muahahaha 
//diag_log format ["[%1] disabling respawn button", time];
//_null = call compile preProcessFileLineNumbers (fp_path+"func\noMoreRespawn.sqf");  // disabled for now, has issues with load time


// Required for JTK Suppressors
execVM "JTK\suppressors\init.sqf";

// ACRE AI Listening
execVM "lib\enemy\acre_talk.sqf";


// setup EOD stuff
diag_log format ["[%1] Setting up EOD", time];
eod_ambientBombers setVariable ["reezo_eod_probability", 0.7];
eod_ambientBombers setVariable ["reezo_eod_interval", 120];
eod_ambientBombers setVariable ["reezo_eod_range",[100,300]];


diag_log format ["[%1] Player enters world!", time];


if ((!isServer) && (player != player)) then {
  waitUntil {player == player};
};

diag_log format ["[%1] Loading player pre init", time];
#include "lib\player_pre_init.sqf";


diag_log format ["[%1] Loading Everybody Dead Checker", time];
execVM "lib\loops\everybodyDead.sqf";

diag_log format ["[%1] Loading Connected Players Checker", time];
execVM "lib\loops\connectedPlayers.sqf";

// Give Players Gear
diag_log format ["[%1] Gearing players", time];
gearPlayers = compile preProcessFileLineNumbers (fp_path+"ammo\gearPlayers.sqf"); 
call gearPlayers;


// Party trigger
call compile preProcessFileLineNumbers (fp_path+"partyTrigger.sqf");


processInitCommands;
finishMissionInit;

// client stuff after finishMissionInit

execVM "lib\init_client.sqf";

// EOF