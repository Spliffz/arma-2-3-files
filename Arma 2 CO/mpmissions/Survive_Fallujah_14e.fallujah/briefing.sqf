//#######################################
//
//	Mission: Survive Fallujah 2013
//  Map: Fallujah
//  Players: 8
//  Author: Spliffz <thespliffz@gmail.com>
//
//	A Spliffz Production 2013
//	
//  Ammobox creator - cp_ammobox.sqf
//
//#######################################

if(isDedicated) exitWith{};

player createDiarySubject ["sf1","Survive Fallujah"];
player createDiarySubject ["sf2","Credits"];

player createDiaryRecord ["sf2", ["Credits", "
<br/>
<br/>
Survive Fallujah - A Spliffz Production<br/>
<br/>
<br/>
Engima of Ostgota Ops - For making Escape Chernarus (played this over and over, still do), which inspired me to make this.<br/>
ArctorKovich - For all the testing, bouncing ideas, problems and solutions.<br/>
Sacha[TDNL] - For all the testing, bouncing ideas, problems and solutions.<br/>
Roy86 - For making Patrol Ops, from what I learned a lot!<br/>
Dutch Tactical Team [DTT] - For the endless support and motivation!<br/>
Shezan74 - For making the Fallujah map (it's just so awesome)<br/>
<br/>
<br/>
[Testers]<br/>
Arctor<br/>
MOKER<br/>
Gerben127<br/>
DarkForce<br/>
NuckFuts<br/>
Ray<br/>
Pek/DistortionNL<br/>
Archu<br/>
TomH<br/>
<br/>
<br/>
Author Contact Info:<br/>
Email/Skype: thespliffz@gmail.com<br/>
TS: try tangodown.nl or dutchtacticalteam.nl
"]];


player createDiaryRecord ["sf1", [ "About" , "
Survive Fallujah is a co-op survival game mode for 8 players.<br/>
<br/>
You got one objective: Find your way to FOB Holland.<br/>
Oh and, try to stay alive.<br/>
"]];


//if(true) exitWith{};

// EOF