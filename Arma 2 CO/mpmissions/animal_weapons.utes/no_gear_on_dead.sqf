/*
	Simple Gear Remove From Dead Script v1.2
	By Spliffz <thespliffz@gmail.com>
	
	
	Options:
	There are some optional settings. The format for these are:
	script = [this, Init] execVM "vehicle.sqf"

	
	[To use]
	Put this in the init field of the unit:
	script = [this] execVM "no_gear_on_dead.sqf";
	
	To use the init field:
	script = [this, "here comes the init"] execVM "no_gear_on_dead.sqf";
	[EXAMPLE]
	script = [this, "this addWeapon 'M16A2'"] execVM "no_gear_on_dead.sqf";
	
	That's all.
	
	When that unit dies, he will lose all of his gear.
*/

if (!isServer) exitWith {};

// vars
_unit = _this select 0;
_hasname = false;
_unitname = vehicleVarName _unit;
_unitinit = if (count _this > 1) then {_this select 1} else {};
_haveinit = if (count _this > 1) then {true} else {false};

if (isNil _unitname) then {_hasname = false;} else {_hasname = true;};
_run = true;


while {_run} do 
{
	sleep 2;
	if(!alive _unit) then {
		removeAllWeapons _unit;
		removeAllItems _unit;
		if (_haveinit) then {
			_unit setVehicleInit format ["%1;", _unitinit];
			processInitCommands;
		};
	};
};