//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////

if(isServer) exitWith {};

// CONFIG

fDir = "functions\";


// EVENTHANDLERS
_array = ["harrier1", "player"];

{
	_firedEH = player addEventHandler ["FIRED", { 
		_this execVM fDir+"chickenBullet.sqf";
	}];
} forEach _array;


_firedEHVeh = helo1 addEventHandler ["FIRED", { 
    _this execVM fDir+"chickenBullet.sqf";
}];


// REST
//execVM "rabbit.sqf";

player addAction ["Para-Sheep", "para_sheep.sqf"];
player addAction ["Para-Sheep 2", "drop_sheep_loc.sqf"];
player addAction ["Drop Rabbit", "rabbit.sqf"];


// EOF