
animal = _this select 0;


_animal = createVehicle ["Sheep", _position, [], 0, "NONE"];
_animal setPos [ _positionX + _a, _positionY, (_positionZ) + 100];

_chute = createVehicle ["ParachuteMediumWest_EP1", position _animal, [], 0, "NONE"];
_chute attachTo [_animal,[0,0,0.5]];

_a = _a + 10;

_x = 1;
while {_x < 2} do {
    if (getPos _animal select 2 < 2) then {
        detach _chute;
        _expl = "Grenade" createVehicle (getPos _animal);
        _x = 3;
    };
};

