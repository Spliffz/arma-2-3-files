private ["_tempwepconfigname","_weapon","_cfgweapons","_magazine","_cfgmagazines"];

RSLO_wl_master = [];

_cfgweapons = configFile >> "cfgWeapons";

for "_i" from 0 to (count _cfgweapons)-1 do { 
    
    _weapon = _cfgweapons select _i;
    
    if (isClass _weapon) then {
        _tempwepconfigname = configName(_weapon);
        
        RSLO_wl_master = RSLO_wl_master + [_tempwepconfigname];
        
        hintSilent format ["Scaning weapon %1", _i];
    };
    

};    
        
_cfgmagazines = configFile >> "cfgmagazines";

for "_i" from 0 to (count _cfgmagazines)-1 do {
    
	_magazine = _cfgmagazines select _i;
    
	if (isClass _magazine) then {
        
        _tempmagconfigname = configName(_magazine);
        
        RSLO_wl_master = RSLO_wl_master + [_tempmagconfigname];        
        
                hintSilent format ["Scaning mag %1", _i];
    };
    
    
};

publicVariable "RSLO_wl_master"; 
    