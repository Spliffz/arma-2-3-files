﻿
RSLO_wl_active = true;

if (isServer) then { _nulll = [] execVM "RSLO\RSLO_server_masterwl.sqf";};

//If set to true, RSLO will use the master whitelist and ignore the numbered groups.
//Assuming the whitelist finctionality is working properly, using this setting
//is basically a way to globally restrict what gear can be aquired with RSLO
//without having to spend time making sure all players are put into a whitelist group.
//
//NOTE: You can NOT use the master whitelist and the numbered whitelists at the same time.
//
//NOTE: If you would like to automatically add gear from gear boxes to the master whitelist,
//you can attach RSLO_master_whitelist.sqf to a gear box and set it to run after the gear
//box is filled. If you run this sqf on multiple gear boxes, it will automacially append
//the contents of each gear box to the existing list. You can also define a master list
//here and then run it on item boxes to ensure everything is in the list. Make sure you
//pass "this" as a parameter to RSLO_master_whitelist.sqf where "this" is the gear box.
RSLO_wl_use_master = true;

RSLO_wl_master = [];

RSLO_wl_1 = [];
RSLO_wl_2 = [];
RSLO_wl_3 = [];
RSLO_wl_4 = [];
RSLO_wl_5 = [];
RSLO_wl_6 = [];
RSLO_wl_7 = [];
RSLO_wl_8 = [];
RSLO_wl_9 = [];
RSLO_wl_10 = [];
RSLO_wl_11 = [];
RSLO_wl_12 = [];
RSLO_wl_13 = [];
RSLO_wl_14 = [];
RSLO_wl_15 = [];



RSLO_wl_groups = [RSLO_wl_1,RSLO_wl_2,RSLO_wl_3,RSLO_wl_4,RSLO_wl_5,RSLO_wl_6,RSLO_wl_7,RSLO_wl_8,RSLO_wl_9,RSLO_wl_10,RSLO_wl_11,RSLO_wl_12,RSLO_wl_13,RSLO_wl_14,RSLO_wl_15];