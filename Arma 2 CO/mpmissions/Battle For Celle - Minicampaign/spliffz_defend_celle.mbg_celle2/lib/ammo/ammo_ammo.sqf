//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////
//## rifles filler

if (isServer) exitWith{};

_timer = 900;
_amountAmmo = 300;
_amountWeapons = 20;

while {alive _this} do
{

	//MISC guns
	_this addMagazineCargo ["8Rnd_B_Beneli_74Slug", _amountAmmo];
	_this addMagazineCargo ["8Rnd_B_Beneli_Pellets", _amountAmmo];
	_this addMagazineCargo ["ACE_8Rnd_12Ga_Buck00", _amountAmmo];
	_this addMagazineCargo ["ACE_8Rnd_12Ga_Slug", _amountAmmo];
	_this addMagazineCargo ["ACE_13Rnd_9x19_L9A1", _amountAmmo];
	_this addMagazineCargo ["ACE_SSGreen_FG", _amountAmmo];
	_this addMagazineCargo ["ACE_SSRed_FG", _amountAmmo];
	_this addMagazineCargo ["ACE_SSWhite_FG", _amountAmmo];
	_this addMagazineCargo ["ACE_SSYellow_FG", _amountAmmo];
	_this addMagazineCargo ["17Rnd_9x19_glock17", _amountAmmo];
	_this addMagazineCargo ["ACE_33Rnd_9x19_G18", _amountAmmo];
	_this addMagazineCargo ["6Rnd_45ACP", _amountAmmo];

	_this addMagazineCargo ["ACE_15Rnd_9x19_S_M9", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_9x19_S_UZI", _amountAmmo];
	_this addMagazineCargo ["30Rnd_9x19_MP5SD", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_9x19_S_MP5", _amountAmmo];
	_this addMagazineCargo ["ACE_40Rnd_B_46x30_MP7", _amountAmmo];

	_this addMagazineCargo ["ACE_FlareIR_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_SSWhite_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_SSYellow_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_SSGreen_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_SSRed_M203", _amountAmmo];

	_this addMagazineCargo ["1Rnd_Smoke_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_SmokeRed_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_SmokeGreen_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_SmokeYellow_M203", _amountAmmo];
	_this addMagazineCargo ["SmokeShell", _amountAmmo];
	_this addMagazineCargo ["SmokeShellBlue", _amountAmmo];
	_this addMagazineCargo ["SmokeShellGreen", _amountAmmo];
	_this addMagazineCargo ["SmokeShellOrange", _amountAmmo];
	_this addMagazineCargo ["SmokeShellPurple", _amountAmmo];
	_this addMagazineCargo ["SmokeShellRed", _amountAmmo];
	_this addMagazineCargo ["SmokeShellYellow", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_SB_S_Stanag", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_T_Stanag", _amountAmmo];
	_this addMagazineCargo ["100Rnd_762x51_M240", _amountAmmo];
	_this addMagazineCargo ["HandGrenade_West", _amountAmmo];
	_this addMagazineCargo ["FlareWhite_M203", _amountAmmo];
	_this addMagazineCargo ["FlareYellow_M203", _amountAmmo];
	_this addMagazineCargo ["FlareGreen_M203", _amountAmmo];
	_this addMagazineCargo ["FlareRed_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_Flashbang", _amountAmmo];
	_this addMagazineCargo ["ACE_1Rnd_HE_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_HE_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_1Rnd_CS_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_25Rnd_1143x23_B_UMP45", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_HK417", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_G3", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_G36", _amountAmmo];

    sleep _timer;
}; 


// EOF
