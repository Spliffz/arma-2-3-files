//////////////////////////////////////////////////
//         A Spliffz Production - 2013
//////////////////////////////////////////////////

titleCut ["", "BLACK FADED", 999];

// Revive
//execVM "revive\ReviveAceWounds.sqf";
ace_sys_wounds_no_medical_gear = true;

player switchMove "amovpercmstpslowwrfldnon_player_idlesteady03";  //lower players weapon
0 fadeRadio 0;


if !(player hasWeapon "ACE_Earplugs") then {player addWeapon "ACE_Earplugs";};

if(side player == west) then {
	[] Spawn {
		waitUntil{!(isNil "BIS_fnc_init")};
		titleText ["Mini-Campaign: The Battle for Celle","PLAIN DOWN"]; 
		titleFadeOut 8;
		sleep 6;
		titleText ["Part 3: The Endgame","PLAIN DOWN"]; 
		titleFadeOut 8;
		sleep 8;
		titleText ["By Spliffz[TDNL]","PLAIN DOWN"]; 
		titleFadeOut 8;
		sleep 3;

		// Info text
		[str ("Deutschland"), str("Flugplatz Fassberg"), str(date select 1) + "." + str(date select 2) + "." + str(date select 0)] spawn BIS_fnc_infoText;
		titleCut ["", "BLACK IN", 10];
	};
};

if(side player == east) then {
	[] Spawn {
		waitUntil{!(isNil "BIS_fnc_init")};
		titleText ["Mini-Campaign: The Battle for Celle","PLAIN DOWN"]; 
		titleFadeOut 8;
		sleep 6;
		titleText ["Part 3: The Endgame","PLAIN DOWN"]; 
		titleFadeOut 8;
		sleep 8;
		titleText ["By Spliffz[TDNL]","PLAIN DOWN"]; 
		titleFadeOut 8;
		sleep 3;

		// Info text
		[str ("Germany"), str("Celle Airfield"), str(date select 1) + "." + str(date select 2) + "." + str(date select 0)] spawn BIS_fnc_infoText;
		titleCut ["", "BLACK IN", 10];
	};
};

// EOF