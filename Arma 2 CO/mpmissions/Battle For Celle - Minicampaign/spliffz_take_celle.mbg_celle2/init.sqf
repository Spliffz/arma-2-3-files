//////////////////////////////////////////////////
//         A Spliffz Production - 2013
//////////////////////////////////////////////////

titleCut ["", "BLACK FADED", 999];

if ((!isServer) && (player != player)) then
{
  waitUntil {player == player};
};

KRON_UPS_INIT = 0;
call compile preprocessFileLineNumbers "scripts\Init_UPSMON.sqf";
processInitCommands;
finishMissionInit;

// Revive
execVM "revive\ReviveAceWounds.sqf";

// ACRE AI Listening
execVM "lib\enemy\acre_talk.sqf";


[] execVM "lib\server.sqf";

[] execVM "lib\client.sqf";


diag_log format ["spectator parameter: %1", paramsArray select 2];

// EOF