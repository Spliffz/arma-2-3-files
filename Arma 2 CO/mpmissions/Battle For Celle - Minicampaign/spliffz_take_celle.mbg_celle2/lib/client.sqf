//////////////////////////////////////////////////
//         A Spliffz Production - 2013
//////////////////////////////////////////////////

if !(player hasWeapon "ACE_Earplugs") then { player addWeapon "ACE_Earplugs"; };
player switchMove "amovpercmstpslowwrfldnon_player_idlesteady03";  
0 fadeRadio 0;

[] Spawn {
	waitUntil{!(isNil "BIS_fnc_init")};
	titleText ["Mini-Campaign: The Battle for Celle","PLAIN DOWN"]; 
	titleFadeOut 8;
	sleep 6;
	titleText ["Part 1: The Takeover","PLAIN DOWN"]; 
	titleFadeOut 8;
	sleep 8;
	titleText ["By Spliffz[TDNL]","PLAIN DOWN"]; 
	titleFadeOut 3;
	sleep 3;

	// Info text
	[str ("Germany"), str("12Km north of Celle"), str(date select 1) + "." + str(date select 2) + "." + str(date select 0)] spawn BIS_fnc_infoText;
	titleCut ["", "BLACK IN", 10];
};


// EOF