//////////////////////////////////////////////////
//         A Spliffz Production - 2013
//////////////////////////////////////////////////

// nul = [this] execVM "lib\ammo\car_medical.sqf";

_car = _this select 0;
_amountAmmo = 80;
_timer = 900;

//clearWeaponCargo _car;
//clearMagazineCargo _car;

_car addMagazineCargo ["ACE_LargeBandage", _amountAmmo];
_car addMagazineCargo ["ACE_Morphine", _amountAmmo];
_car addMagazineCargo ["ACE_Epinephrine", _amountAmmo];
_car addMagazineCargo ["ACE_Medkit", _amountAmmo];

//sleep _timer;

// EOF