//////////////////////////////////////////////////
//         A Spliffz Production - 2013
//////////////////////////////////////////////////
//## rifles filler

//if (!isServer && !isDedicated) exitWith{};
//if (isServer) exitWith{};
//if (!isServer) exitWith{};

//nul = [this] execVM "lib\ammo\wpn_russians.sqf";

_car = _this select 0;
_timer = 900;
_amountAmmo = 100;
_amountWeapons = 20;

clearWeaponCargo _car;
clearMagazineCargo _car;

_car addMagazineCargo ["ACE_Rope_TOW_M_5", 10];

// Rifles
// Weapons
_car addWeaponCargo ["ACE_AEK_971", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_971_1p63", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_971_1p78", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_971_1pn100", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_971_gp", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_971_gp_1p63", _amountWeapons];
//_car addWeaponCargo ["ACE_AEK_971_shahin", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_971_tgp_cln", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_973s", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_973s_1p63", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_973s_1p78", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_973s_1pn100", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_973s_gp", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_973s_gp_1p63", _amountWeapons];
//_car addWeaponCargo ["ACE_AEK_973s_shahin", _amountWeapons];
_car addWeaponCargo ["ACE_AEK_973s_tgp_cln", _amountWeapons];
_car addWeaponCargo ["ACE_AK103", _amountWeapons];
_car addWeaponCargo ["ACE_AK103_1P29", _amountWeapons];
_car addWeaponCargo ["ACE_AK103_GL", _amountWeapons];
_car addWeaponCargo ["ACE_AK103_GL_1P29", _amountWeapons];
_car addWeaponCargo ["ACE_AK103_GL_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_AK103_GL_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_AK103_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_AK103_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_AK104", _amountWeapons];
_car addWeaponCargo ["ACE_AK104_1P29", _amountWeapons];
_car addWeaponCargo ["ACE_AK104_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_AK104_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_AK105", _amountWeapons];
_car addWeaponCargo ["ACE_AK105_1P29", _amountWeapons];
_car addWeaponCargo ["ACE_AK105_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_AK105_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_1P29", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_1P78", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_1P78_FL", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_1P78_FL_F", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_FL", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_FL_F", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_GL", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_GL_1P29", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_GL_1P78", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_GL_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_GL_NSPU", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_GL_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_GL_TWS", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_Kobra_FL", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_Kobra_FL_F", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_NSPU", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_NSPU_FL", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_NSPU_FL_F", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_PSO_FL", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_PSO_FL_F", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD_1P78", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD_1P78_F", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD_F", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD_Kobra_F", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD_NSPU", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD_NSPU_F", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_AK74M_SD_PSO_F", _amountWeapons];
//_car addWeaponCargo ["ACE_AK74M_SD_TWS", _amountWeapons];
//_car addWeaponCargo ["ACE_AK74M_SD_TWS_F", _amountWeapons];
//_car addWeaponCargo ["ACE_AK74M_TWS", _amountWeapons];
//_car addWeaponCargo ["ACE_AK74M_TWS_FL", _amountWeapons];
//_car addWeaponCargo ["ACE_AK74M_TWS_FL_F", _amountWeapons];
_car addWeaponCargo ["ACE_AKM", _amountWeapons];
_car addWeaponCargo ["ACE_AKM_GL", _amountWeapons];
_car addWeaponCargo ["ACE_AKMS", _amountWeapons];
_car addWeaponCargo ["ACE_AKMS_SD", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74_GP25", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74_UN", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74P", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74P_1P29", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74P_GL", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74P_GL_1P29", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74P_GL_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74P_GL_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74P_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_AKS74P_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_EVO3_CCO_F", _amountWeapons];
_car addWeaponCargo ["ACE_EVO3_SD_CCO_L", _amountWeapons];
_car addWeaponCargo ["ACE_KAC_PDW", _amountWeapons];
_car addWeaponCargo ["ACE_Minedetector_US", _amountWeapons];
_car addWeaponCargo ["ACE_SKS", _amountWeapons];
_car addWeaponCargo ["ACE_Val", _amountWeapons];
_car addWeaponCargo ["ACE_Val_Kobra", _amountWeapons];
_car addWeaponCargo ["ACE_Val_PSO", _amountWeapons];
_car addWeaponCargo ["ACE_VMH3", _amountWeapons];
_car addWeaponCargo ["ACE_VMM3", _amountWeapons];
_car addWeaponCargo ["AK_107_GL_kobra", _amountWeapons];
_car addWeaponCargo ["AK_107_GL_pso", _amountWeapons];
_car addWeaponCargo ["AK_107_kobra", _amountWeapons];
_car addWeaponCargo ["AK_107_pso", _amountWeapons];
_car addWeaponCargo ["AK_47_M", _amountWeapons];
_car addWeaponCargo ["AK_47_S", _amountWeapons];
_car addWeaponCargo ["AK_74", _amountWeapons];
_car addWeaponCargo ["AK_74_GL", _amountWeapons];
_car addWeaponCargo ["AK_74_GL_kobra", _amountWeapons];
_car addWeaponCargo ["AKS_74", _amountWeapons];
_car addWeaponCargo ["AKS_74_GOSHAWK", _amountWeapons];
_car addWeaponCargo ["AKS_74_kobra", _amountWeapons];
_car addWeaponCargo ["AKS_74_NSPU", _amountWeapons];
_car addWeaponCargo ["AKS_74_pso", _amountWeapons];
_car addWeaponCargo ["AKS_74_U", _amountWeapons];
_car addWeaponCargo ["AKS_74_UN_kobra", _amountWeapons];
//_car addWeaponCargo ["AKS_GOLD", _amountWeapons];
_car addWeaponCargo ["bizon", _amountWeapons];
_car addWeaponCargo ["bizon_silenced", _amountWeapons];
_car addWeaponCargo ["Evo_ACR", _amountWeapons];
_car addWeaponCargo ["Evo_mrad_ACR", _amountWeapons];
_car addWeaponCargo ["evo_sd_ACR", _amountWeapons];
_car addWeaponCargo ["FN_FAL", _amountWeapons];
_car addWeaponCargo ["FN_FAL_ANPVS4", _amountWeapons];
_car addWeaponCargo ["LeeEnfield", _amountWeapons];
_car addWeaponCargo ["M1014", _amountWeapons];
_car addWeaponCargo ["Sa58P_EP1", _amountWeapons];
_car addWeaponCargo ["Sa58V_CCO_EP1", _amountWeapons];
_car addWeaponCargo ["Sa58V_EP1", _amountWeapons];
_car addWeaponCargo ["Sa58V_RCO_EP1", _amountWeapons];
_car addWeaponCargo ["Saiga12K", _amountWeapons];

// Magazines
_car addMagazineCargo ["30Rnd_545x39_AK", _amountAmmo];
_car addMagazineCargo ["30Rnd_545x39_AKSD", _amountAmmo];
_car addMagazineCargo ["75Rnd_545x39_RPK", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_545x39_AP_AK", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_545x39_EP_AK", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_545x39_T_AK", _amountAmmo];
_car addMagazineCargo ["ACE_45Rnd_545x39_AP_AK", _amountAmmo];
_car addMagazineCargo ["ACE_45Rnd_545x39_B_AK", _amountAmmo];
_car addMagazineCargo ["ACE_45Rnd_545x39_EP_AK", _amountAmmo];
_car addMagazineCargo ["ACE_75Rnd_545x39_T_RPK", _amountAmmo];
_car addMagazineCargo ["1Rnd_HE_GP25", _amountAmmo];
_car addMagazineCargo ["1Rnd_SMOKE_GP25", _amountAmmo];
_car addMagazineCargo ["1Rnd_SmokeGreen_GP25", _amountAmmo];
_car addMagazineCargo ["1Rnd_SmokeRed_GP25", _amountAmmo];
_car addMagazineCargo ["1Rnd_SmokeYellow_GP25", _amountAmmo];
_car addMagazineCargo ["ACE_1Rnd_CS_GP25", _amountAmmo];
_car addMagazineCargo ["ACE_1Rnd_HE_GP25P", _amountAmmo];
_car addMagazineCargo ["ACE_SSGreen_GP25", _amountAmmo];
_car addMagazineCargo ["ACE_SSRed_GP25", _amountAmmo];
_car addMagazineCargo ["ACE_SSWhite_GP25", _amountAmmo];
_car addMagazineCargo ["ACE_SSYellow_GP25", _amountAmmo];
_car addMagazineCargo ["FlareGreen_GP25", _amountAmmo];
_car addMagazineCargo ["FlareRed_GP25", _amountAmmo];
_car addMagazineCargo ["FlareWhite_GP25", _amountAmmo];
_car addMagazineCargo ["FlareYellow_GP25", _amountAmmo];
_car addMagazineCargo ["30Rnd_762x39_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_762x39_AP_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_762x39_AP_S_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_762x39_SD_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_762x39_T_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_40Rnd_762x39_AP_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_40Rnd_762x39_B_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_40Rnd_762x39_T_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_75Rnd_762x39_B_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_75Rnd_762x39_SD_AK47", _amountAmmo];
_car addMagazineCargo ["20Rnd_9x19_EVO", _amountAmmo];
_car addMagazineCargo ["20Rnd_9x19_EVOSD", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_6x35_B_PDW", _amountAmmo];
_car addMagazineCargo ["8Rnd_B_Beneli_74Slug", _amountAmmo];
_car addMagazineCargo ["8Rnd_B_Beneli_Pellets", _amountAmmo];
_car addMagazineCargo ["ACE_20Rnd_9x39_B_OC14", _amountAmmo];
_car addMagazineCargo ["ACE_20Rnd_9x39_B_SP6_OC14", _amountAmmo];
_car addMagazineCargo ["10Rnd_9x39_SP5_VSS", _amountAmmo];
_car addMagazineCargo ["20Rnd_9x39_SP5_VSS", _amountAmmo];
_car addMagazineCargo ["ACE_10Rnd_9x39_SP6_VSS", _amountAmmo];
_car addMagazineCargo ["ACE_20Rnd_9x39_SP6_VSS", _amountAmmo];
_car addMagazineCargo ["64Rnd_9x19_Bizon", _amountAmmo];
_car addMagazineCargo ["64Rnd_9x19_SD_Bizon", _amountAmmo];
_car addMagazineCargo ["20Rnd_762x51_FNFAL", _amountAmmo];
_car addMagazineCargo ["ACE_20Rnd_762x51_B_FAL", _amountAmmo];
_car addMagazineCargo ["ACE_20Rnd_762x51_T_FAL", _amountAmmo];
_car addMagazineCargo ["ACE_10Rnd_762x39_B_SKS", _amountAmmo];
_car addMagazineCargo ["ACE_10Rnd_762x39_T_SKS", _amountAmmo];
_car addMagazineCargo ["10x_303", _amountAmmo];
_car addMagazineCargo ["ACE_10Rnd_77x56_T_SMLE", _amountAmmo];
_car addMagazineCargo ["30Rnd_762x39_SA58", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_762x39_T_SA58", _amountAmmo];
_car addMagazineCargo ["8Rnd_B_Saiga12_74Slug", _amountAmmo];
_car addMagazineCargo ["8Rnd_B_Saiga12_Pellets", _amountAmmo];

// AR
// Weapons
_car addWeaponCargo ["ACE_RPK", _amountWeapons];
_car addWeaponCargo ["ACE_RPK74M", _amountWeapons];
_car addWeaponCargo ["ACE_RPK74M_1P29", _amountWeapons];
_car addWeaponCargo ["RPK_74", _amountWeapons];

// Magazines
_car addMagazineCargo ["30Rnd_762x39_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_762x39_AP_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_762x39_AP_S_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_762x39_SD_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_762x39_T_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_40Rnd_762x39_AP_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_40Rnd_762x39_B_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_40Rnd_762x39_T_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_75Rnd_762x39_B_AK47", _amountAmmo];
_car addMagazineCargo ["ACE_75Rnd_762x39_SD_AK47", _amountAmmo];
_car addMagazineCargo ["30Rnd_545x39_AK", _amountAmmo];
_car addMagazineCargo ["30Rnd_545x39_AKSD", _amountAmmo];
_car addMagazineCargo ["75Rnd_545x39_RPK", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_545x39_AP_AK", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_545x39_EP_AK", _amountAmmo];
_car addMagazineCargo ["ACE_30Rnd_545x39_T_AK", _amountAmmo];
_car addMagazineCargo ["ACE_45Rnd_545x39_AP_AK", _amountAmmo];
_car addMagazineCargo ["ACE_45Rnd_545x39_B_AK", _amountAmmo];
_car addMagazineCargo ["ACE_45Rnd_545x39_EP_AK", _amountAmmo];
_car addMagazineCargo ["ACE_75Rnd_545x39_T_RPK", _amountAmmo];



// Launchers
// Weapons
_car addWeaponCargo ["ACE_RMG", _amountWeapons];
_car addWeaponCargo ["ACE_RPG22", _amountWeapons];
_car addWeaponCargo ["ACE_RPG27", _amountWeapons];
_car addWeaponCargo ["ACE_RPG29", _amountWeapons];
_car addWeaponCargo ["ACE_RPG7V_PGO7", _amountWeapons];
_car addWeaponCargo ["ACE_RPOM", _amountWeapons];
_car addWeaponCargo ["ACE_RSHG1", _amountWeapons];
_car addWeaponCargo ["Igla", _amountWeapons];
_car addWeaponCargo ["MetisLauncher", _amountWeapons];
_car addWeaponCargo ["RPG18", _amountWeapons];
_car addWeaponCargo ["RPG7V", _amountWeapons];
_car addWeaponCargo ["Strela", _amountWeapons];

// Magazines
_car addMagazineCargo ["ACE_RPG29_PG29", _amountAmmo];
_car addMagazineCargo ["ACE_RPG29_TBG29", _amountAmmo];
_car addMagazineCargo ["ACE_PG7VM", _amountAmmo];
_car addMagazineCargo ["ACE_TBG7V", _amountAmmo];
_car addMagazineCargo ["OG7", _amountAmmo];
_car addMagazineCargo ["PG7V", _amountAmmo];
_car addMagazineCargo ["PG7VL", _amountAmmo];
_car addMagazineCargo ["PG7VR", _amountAmmo];
_car addMagazineCargo ["Igla", _amountAmmo];
_car addMagazineCargo ["ACE_AT13TB", _amountAmmo];
_car addMagazineCargo ["AT13", _amountAmmo];
_car addMagazineCargo ["Strela", _amountAmmo];


// MG
// Weapons
_car addWeaponCargo ["Pecheneg", _amountWeapons];
_car addWeaponCargo ["PK", _amountWeapons];

// Magazines
_car addMagazineCargo ["100Rnd_762x54_PK", _amountAmmo];



// Pistols
// Weapons
_car addWeaponCargo ["ACE_APS", _amountWeapons];
_car addWeaponCargo ["ACE_APSB", _amountWeapons];
_car addWeaponCargo ["ACE_Flaregun", _amountWeapons];
_car addWeaponCargo ["ACE_Knicklicht_Proxy", _amountWeapons];
_car addWeaponCargo ["ACE_MugLite", _amountWeapons];
_car addWeaponCargo ["ACE_P226", _amountWeapons];
_car addWeaponCargo ["ACE_P8", _amountWeapons];
_car addWeaponCargo ["ACE_SearchMirror", _amountWeapons];
_car addWeaponCargo ["ACE_SSVZ", _amountWeapons];
_car addWeaponCargo ["CZ_75_D_COMPACT", _amountWeapons];
_car addWeaponCargo ["CZ_75_P_07_DUTY", _amountWeapons];
_car addWeaponCargo ["CZ_75_SP_01_PHANTOM", _amountWeapons];
_car addWeaponCargo ["CZ_75_SP_01_PHANTOM_SD", _amountWeapons];
_car addWeaponCargo ["Makarov", _amountWeapons];
_car addWeaponCargo ["MakarovSD", _amountWeapons];
_car addWeaponCargo ["revolver_EP1", _amountWeapons];
_car addWeaponCargo ["Sa61_EP1", _amountWeapons];
_car addWeaponCargo ["UZI_EP1", _amountWeapons];
_car addWeaponCargo ["UZI_SD_EP1", _amountWeapons];

// Magazines
_car addMagazineCargo ["ACE_20Rnd_9x18_APS", _amountAmmo];
_car addMagazineCargo ["ACE_20Rnd_9x18_APSB", _amountAmmo];
_car addMagazineCargo ["ACE_SSGreen_FG", _amountAmmo];
_car addMagazineCargo ["ACE_SSRed_FG", _amountAmmo];
_car addMagazineCargo ["ACE_SSWhite_FG", _amountAmmo];
_car addMagazineCargo ["ACE_SSYellow_FG", _amountAmmo];
_car addMagazineCargo ["ACE_15Rnd_9x19_P226", _amountAmmo];
_car addMagazineCargo ["ACE_15Rnd_9x19_P8", _amountAmmo];
_car addMagazineCargo ["ACE_8Rnd_762x25_B_Tokarev", _amountAmmo];
_car addMagazineCargo ["18Rnd_9x19_Phantom", _amountAmmo];
_car addMagazineCargo ["18Rnd_9x19_PhantomSD", _amountAmmo];
_car addMagazineCargo ["ACE_16Rnd_9x19_CZ75", _amountAmmo];
_car addMagazineCargo ["8Rnd_9x18_Makarov", _amountAmmo];
_car addMagazineCargo ["8Rnd_9x18_MakarovSD", _amountAmmo];
_car addMagazineCargo ["6Rnd_45ACP", _amountAmmo];
_car addMagazineCargo ["10Rnd_B_765x17_Ball", _amountAmmo];
_car addMagazineCargo ["20Rnd_B_765x17_Ball", _amountAmmo];
_car addMagazineCargo ["30Rnd_9x19_UZI", _amountAmmo];
_car addMagazineCargo ["30Rnd_9x19_UZI_SD", _amountAmmo];


// Sniper
// Weapons
_car addWeaponCargo ["ACE_SVD_Bipod", _amountWeapons];
_car addWeaponCargo ["CZ_750_S1_ACR", _amountWeapons];
_car addWeaponCargo ["huntingrifle", _amountWeapons];
_car addWeaponCargo ["ksvk", _amountWeapons];
_car addWeaponCargo ["SVD", _amountWeapons];
_car addWeaponCargo ["SVD_CAMO", _amountWeapons];
_car addWeaponCargo ["SVD_des_EP1", _amountWeapons];
_car addWeaponCargo ["SVD_NSPU_EP1", _amountWeapons];
_car addWeaponCargo ["VSS_vintorez", _amountWeapons];

// Magazines
_car addMagazineCargo ["10Rnd_762x54_SVD", _amountAmmo];
_car addMagazineCargo ["ACE_10Rnd_762x54_T_SVD", _amountAmmo];
_car addMagazineCargo ["5x_22_LR_17_HMR", _amountAmmo];
_car addMagazineCargo ["5Rnd_127x108_KSVK", _amountAmmo];
_car addMagazineCargo ["ACE_5Rnd_127x108_T_KSVK", _amountAmmo];
_car addMagazineCargo ["10Rnd_9x39_SP5_VSS", _amountAmmo];
_car addMagazineCargo ["20Rnd_9x39_SP5_VSS", _amountAmmo];
_car addMagazineCargo ["ACE_10Rnd_9x39_SP6_VSS", _amountAmmo];
_car addMagazineCargo ["ACE_20Rnd_9x39_SP6_VSS", _amountAmmo];


// EOF