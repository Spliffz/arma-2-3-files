//////////////////////////////////////////////////
//         A Spliffz Production - _amountWeapons01_amountWeapons
//////////////////////////////////////////////////
//## rifles filler

if (!isServer) exitWith{};

_timer = 900;
_amountAmmo = 100;
_amountWeapons = 40;

while {alive _this} do
{

	// Weapons
	_this addWeaponCargo ["ACE_MG36", _amountWeapons];
	_this addWeaponCargo ["ACE_MG36_D", _amountWeapons];
	_this addWeaponCargo ["MG36", _amountWeapons];
	_this addWeaponCargo ["MG36_camo", _amountWeapons];

	// Magazines
	//_this addMagazineCargo ["100Rnd_556x45_BetaCMag", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_Stanag", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_StanagSD", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_SB_Stanag", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_T_Stanag", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_G36", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_G36SD", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_AP_G36", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_T_G36", _amountAmmo];


	// Launchers
	// Weapons
	//_this addWeaponCargo ["ACE_Arty_AimingPost_M1A2_M58", _amountWeapons];
	//_this addWeaponCargo ["ACE_Arty_AimingPost_M1A2_M59", _amountWeapons];
	//_this addWeaponCargo ["ACE_Arty_M1A1_Collimator", _amountWeapons];
	//_this addWeaponCargo ["ACE_Arty_M2A2_Aiming_Circle", _amountWeapons];
	_this addWeaponCargo ["ACE_Javelin_CLU", _amountWeapons];
	_this addWeaponCargo ["ACE_Javelin_Direct", _amountWeapons];
	_this addWeaponCargo ["ACE_M136_CSRS", _amountWeapons];
	_this addWeaponCargo ["ACE_M47_Daysight", _amountWeapons];
	_this addWeaponCargo ["ACE_M72", _amountWeapons];
	_this addWeaponCargo ["ACE_M72A2", _amountWeapons];
	//_this addWeaponCargo ["ACE_ParachutePack", _amountWeapons];
	//_this addWeaponCargo ["ACE_ParachuteRoundPack", _amountWeapons];
	_this addWeaponCargo ["Javelin", _amountWeapons];
	_this addWeaponCargo ["M136", _amountWeapons];
	_this addWeaponCargo ["M47Launcher_EP1", _amountWeapons];
	_this addWeaponCargo ["MAAWS", _amountWeapons];
	_this addWeaponCargo ["SMAW", _amountWeapons];
	_this addWeaponCargo ["Stinger", _amountWeapons];

	// Magazines
	_this addMagazineCargo ["Dragon_EP1", _amountAmmo];
	_this addMagazineCargo ["ACE_MAAWS_HE", _amountAmmo];
	_this addMagazineCargo ["MAAWS_HEAT", _amountAmmo];
	_this addMagazineCargo ["MAAWS_HEDP", _amountAmmo];
	_this addMagazineCargo ["ACE_AT13TB", _amountAmmo];
	_this addMagazineCargo ["AT13", _amountAmmo];
	_this addMagazineCargo ["ACE_SMAW_NE", _amountAmmo];
	_this addMagazineCargo ["SMAW_HEAA", _amountAmmo];
	_this addMagazineCargo ["SMAW_HEDP", _amountAmmo];
	_this addMagazineCargo ["ACE_SMAW_Spotting", _amountAmmo];
	_this addMagazineCargo ["Stinger", _amountAmmo];

	// MG
	// Weapons
	_this addWeaponCargo ["ACE_M240B", _amountWeapons];
	_this addWeaponCargo ["ACE_M240L", _amountWeapons];
	_this addWeaponCargo ["ACE_M240L_M145", _amountWeapons];
	_this addWeaponCargo ["ACE_M60", _amountWeapons];
	_this addWeaponCargo ["M240", _amountWeapons];
	_this addWeaponCargo ["m240_scoped_EP1", _amountWeapons];
	_this addWeaponCargo ["M60A4_EP1", _amountWeapons];

	// Magazines
	_this addMagazineCargo ["100Rnd_762x51_M240", _amountAmmo];



	// Pistols
	// Weapons
	_this addWeaponCargo ["ACE_APS", _amountWeapons];
	_this addWeaponCargo ["ACE_APSB", _amountWeapons];
	_this addWeaponCargo ["ACE_Flaregun", _amountWeapons];
	_this addWeaponCargo ["ACE_Glock18", _amountWeapons];
	_this addWeaponCargo ["ACE_Knicklicht_Proxy", _amountWeapons];
	_this addWeaponCargo ["ACE_MugLite", _amountWeapons];
	_this addWeaponCargo ["ACE_SearchMirror", _amountWeapons];
	_this addWeaponCargo ["ACE_SSVZ", _amountWeapons];
	_this addWeaponCargo ["ACE_TT", _amountWeapons];
	_this addWeaponCargo ["ACE_USP", _amountWeapons];
	_this addWeaponCargo ["ACE_USPSD", _amountWeapons];
	_this addWeaponCargo ["Colt1911", _amountWeapons];
	_this addWeaponCargo ["glock17_EP1", _amountWeapons];
	_this addWeaponCargo ["M9", _amountWeapons];
	_this addWeaponCargo ["M9SD", _amountWeapons];
	_this addWeaponCargo ["revolver_EP1", _amountWeapons];
	_this addWeaponCargo ["UZI_EP1", _amountWeapons];
	_this addWeaponCargo ["UZI_SD_EP1", _amountWeapons];

	// Magazines
	_this addMagazineCargo ["ACE_20Rnd_9x18_APS", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_9x18_APSB", _amountAmmo];
	_this addMagazineCargo ["ACE_SSGreen_FG", _amountAmmo];
	_this addMagazineCargo ["ACE_SSRed_FG", _amountAmmo];
	_this addMagazineCargo ["ACE_SSWhite_FG", _amountAmmo];
	_this addMagazineCargo ["ACE_SSYellow_FG", _amountAmmo];
	_this addMagazineCargo ["17Rnd_9x19_glock17", _amountAmmo];
	_this addMagazineCargo ["ACE_33Rnd_9x19_G18", _amountAmmo];
	_this addMagazineCargo ["ACE_8Rnd_762x25_B_Tokarev", _amountAmmo];
	_this addMagazineCargo ["ACE_12Rnd_45ACP_USP", _amountAmmo];
	_this addMagazineCargo ["7Rnd_45ACP_1911", _amountAmmo];
	_this addMagazineCargo ["15Rnd_9x19_M9", _amountAmmo];
	_this addMagazineCargo ["15Rnd_9x19_M9SD", _amountAmmo];
	_this addMagazineCargo ["6Rnd_45ACP", _amountAmmo];
	_this addMagazineCargo ["10Rnd_B_765x17_Ball", _amountAmmo];
	_this addMagazineCargo ["20Rnd_B_765x17_Ball", _amountAmmo];
	_this addMagazineCargo ["30Rnd_9x19_UZI", _amountAmmo];
	_this addMagazineCargo ["30Rnd_9x19_UZI_SD", _amountAmmo];



	// Rifles
	// Weapons
	//_this addWeaponCargo ["AA12_PMC", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A1_AG36_UP_F", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A1_AG36A1", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A1_AG36A1_D", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A1_AG36A1_D_F", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A1_AG36A1_D_UP", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A1_AG36A1_D_UP_F", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A1_AG36A1_F", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A1_AG36A1_UP", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2_AG36A2", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2_AG36A2_F", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2_AG36A2_UP", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2_AG36A2_UP_F", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2_Bipod", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2_Bipod_D", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2_D", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2_D_F", _amountWeapons];
	_this addWeaponCargo ["ACE_G36A2_F", _amountWeapons];
	_this addWeaponCargo ["ACE_G36K_EOTech", _amountWeapons];
	_this addWeaponCargo ["ACE_G36K_EOTech_D", _amountWeapons];
	_this addWeaponCargo ["ACE_G36K_iron", _amountWeapons];
	_this addWeaponCargo ["ACE_G36K_iron_D", _amountWeapons];
	_this addWeaponCargo ["ACE_G3A3", _amountWeapons];
	_this addWeaponCargo ["ACE_G3A3_RSAS", _amountWeapons];
	_this addWeaponCargo ["ACE_G3A3_RSAS_F", _amountWeapons];
	_this addWeaponCargo ["ACE_G3SG1", _amountWeapons];
	_this addWeaponCargo ["ACE_gr1", _amountWeapons];
	_this addWeaponCargo ["ACE_gr1sd", _amountWeapons];
	_this addWeaponCargo ["ACE_gr1sdsp", _amountWeapons];
	_this addWeaponCargo ["ACE_gr1sp", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D10", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D10_AIM", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D10_COMPM3", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D10_COMPM3_SD", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D10_Holo", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D10_M320", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D10_M320_UP", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D10_SD", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D14", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D14_ACOG_PVS14", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D14_COMPM3", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D14_COMPM3_M320", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D14_COMPM3_M320_UP", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D14_SD", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D14_TWS", _amountWeapons];
	_this addWeaponCargo ["ACE_HK417_Eotech_4x", _amountWeapons];
	_this addWeaponCargo ["ACE_HK417_leupold", _amountWeapons];
	_this addWeaponCargo ["ACE_HK417_micro", _amountWeapons];
	_this addWeaponCargo ["ACE_HK417_Shortdot", _amountWeapons];
	_this addWeaponCargo ["ACE_KAC_PDW", _amountWeapons];
	_this addWeaponCargo ["ACE_M1014_Eotech", _amountWeapons];
	_this addWeaponCargo ["ACE_M27_IAR", _amountWeapons];
	_this addWeaponCargo ["ACE_M27_IAR_ACOG", _amountWeapons];
	_this addWeaponCargo ["ACE_M27_IAR_CCO", _amountWeapons];
	_this addWeaponCargo ["ACE_Minedetector_US", _amountWeapons];
	_this addWeaponCargo ["ACE_MP5A4", _amountWeapons];
	_this addWeaponCargo ["ACE_MP5A5", _amountWeapons];
	_this addWeaponCargo ["ACE_MP5SD", _amountWeapons];
	_this addWeaponCargo ["ACE_MP7", _amountWeapons];
	_this addWeaponCargo ["ACE_MP7_RSAS", _amountWeapons];

	_this addWeaponCargo ["ACE_SPAS12", _amountWeapons];
	_this addWeaponCargo ["ACE_UMP45", _amountWeapons];
	_this addWeaponCargo ["ACE_UMP45_AIM", _amountWeapons];
	_this addWeaponCargo ["ACE_UMP45_AIM_SD", _amountWeapons];
	_this addWeaponCargo ["ACE_UMP45_SD", _amountWeapons];

	_this addWeaponCargo ["G36_C_SD_camo", _amountWeapons];
	_this addWeaponCargo ["G36_C_SD_eotech", _amountWeapons];
	_this addWeaponCargo ["G36a", _amountWeapons];
	_this addWeaponCargo ["G36A_camo", _amountWeapons];
	_this addWeaponCargo ["G36C", _amountWeapons];
	_this addWeaponCargo ["G36C_camo", _amountWeapons];
	_this addWeaponCargo ["G36K", _amountWeapons];
	_this addWeaponCargo ["G36K_camo", _amountWeapons];
	_this addWeaponCargo ["M1014", _amountWeapons];
	_this addWeaponCargo ["MP5A5", _amountWeapons];
	_this addWeaponCargo ["MP5SD", _amountWeapons];

	// Magazines
	_this addMagazineCargo ["20Rnd_B_AA12_74Slug", _amountAmmo];
	_this addMagazineCargo ["20Rnd_B_AA12_HE", _amountAmmo];
	_this addMagazineCargo ["20Rnd_B_AA12_Pellets", _amountAmmo];
	//_this addMagazineCargo ["100Rnd_556x45_BetaCMag", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_G36", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_G36SD", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_AP_G36", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_T_G36", _amountAmmo];
	_this addMagazineCargo ["1Rnd_HE_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_Smoke_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_SmokeGreen_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_SmokeRed_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_SmokeYellow_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_1Rnd_CS_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_1Rnd_HE_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_1Rnd_PR_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_FlareIR_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_HuntIR_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_M576", _amountAmmo];
	_this addMagazineCargo ["ACE_SSGreen_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_SSRed_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_SSWhite_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_SSYellow_M203", _amountAmmo];
	_this addMagazineCargo ["FlareGreen_M203", _amountAmmo];
	_this addMagazineCargo ["FlareRed_M203", _amountAmmo];
	_this addMagazineCargo ["FlareWhite_M203", _amountAmmo];
	_this addMagazineCargo ["FlareYellow_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_B_G3", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_G3", _amountAmmo];
	_this addMagazineCargo ["20Rnd_556x45_Stanag", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_Stanag", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_StanagSD", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_SB_Stanag", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_T_Stanag", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_B_HK417", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_SB_HK417", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_HK417", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_6x35_B_PDW", _amountAmmo];
	_this addMagazineCargo ["8Rnd_B_Beneli_74Slug", _amountAmmo];
	_this addMagazineCargo ["8Rnd_B_Beneli_Pellets", _amountAmmo];
	_this addMagazineCargo ["20Rnd_762x51_DMR", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_B_M14", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_DMR", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_1143x23_B_M3", _amountAmmo];
	_this addMagazineCargo ["30Rnd_9x19_MP5", _amountAmmo];
	_this addMagazineCargo ["30Rnd_9x19_MP5SD", _amountAmmo];
	_this addMagazineCargo ["ACE_40Rnd_B_46x30_MP7", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_9x39_B_OC14", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_9x39_B_SP6_OC14", _amountAmmo];
	_this addMagazineCargo ["20Rnd_762x51_B_SCAR", _amountAmmo];
	_this addMagazineCargo ["20Rnd_762x51_SB_SCAR", _amountAmmo];
	_this addMagazineCargo ["ACE_25Rnd_1143x23_B_UMP45", _amountAmmo];
	_this addMagazineCargo ["10x_303", _amountAmmo];
	_this addMagazineCargo ["ACE_10Rnd_77x56_T_SMLE", _amountAmmo];
	_this addMagazineCargo ["6Rnd_FlareGreen_M203", _amountAmmo];
	_this addMagazineCargo ["6Rnd_FlareRed_M203", _amountAmmo];
	_this addMagazineCargo ["6Rnd_FlareWhite_M203", _amountAmmo];
	_this addMagazineCargo ["6Rnd_FlareYellow_M203", _amountAmmo];
	_this addMagazineCargo ["6Rnd_HE_M203", _amountAmmo];
	_this addMagazineCargo ["6Rnd_Smoke_M203", _amountAmmo];
	_this addMagazineCargo ["6Rnd_SmokeGreen_M203", _amountAmmo];
	_this addMagazineCargo ["6Rnd_SmokeRed_M203", _amountAmmo];
	_this addMagazineCargo ["6Rnd_SmokeYellow_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_6Rnd_CS_M32", _amountAmmo];
	_this addMagazineCargo ["8Rnd_B_Saiga12_74Slug", _amountAmmo];
	_this addMagazineCargo ["8Rnd_B_Saiga12_Pellets", _amountAmmo];


	// Sniper
	// Weapons
	_this addWeaponCargo ["ACE_AS50", _amountWeapons];
	_this addWeaponCargo ["ACE_M109", _amountWeapons];
	_this addWeaponCargo ["ACE_M110", _amountWeapons];
	_this addWeaponCargo ["ACE_M110_SD", _amountWeapons];
	_this addWeaponCargo ["ACE_TAC50", _amountWeapons];
	_this addWeaponCargo ["ACE_TAC50_SD", _amountWeapons];
	_this addWeaponCargo ["DMR", _amountWeapons];
	_this addWeaponCargo ["m107", _amountWeapons];
	_this addWeaponCargo ["m107_TWS_EP1", _amountWeapons];
	_this addWeaponCargo ["M110_NVG_EP1", _amountWeapons];
	_this addWeaponCargo ["M110_TWS_EP1", _amountWeapons];

	// Magazines
	_this addMagazineCargo ["5Rnd_127x99_as50", _amountAmmo];
	_this addMagazineCargo ["ACE_5Rnd_127x99_B_TAC50", _amountAmmo];
	_this addMagazineCargo ["ACE_5Rnd_127x99_T_TAC50", _amountAmmo];
	_this addMagazineCargo ["ACE_5Rnd_25x59_HEDP_Barrett", _amountAmmo];
	_this addMagazineCargo ["20Rnd_762x51_B_SCAR", _amountAmmo];
	_this addMagazineCargo ["20Rnd_762x51_SB_SCAR", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_SB_M110", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_SB_SCAR", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_M110", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_SCAR", _amountAmmo];
	_this addMagazineCargo ["20Rnd_762x51_DMR", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_B_M14", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_DMR", _amountAmmo];
	_this addMagazineCargo ["5x_22_LR_17_HMR", _amountAmmo];
	_this addMagazineCargo ["10Rnd_127x99_m107", _amountAmmo];
	_this addMagazineCargo ["ACE_10Rnd_127x99_Raufoss_m107", _amountAmmo];
	_this addMagazineCargo ["ACE_10Rnd_127x99_T_m107", _amountAmmo];

	sleep _timer;
};

// EOF