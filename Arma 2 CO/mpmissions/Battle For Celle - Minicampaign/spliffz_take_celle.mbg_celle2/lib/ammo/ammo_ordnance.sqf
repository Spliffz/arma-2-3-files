//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////
//## ordnance filler

//if !(isServer) exitWith{};

_timer = 1800;
_amountAmmo = 100;
_amountWeapons = 20;

while {alive _this} do
{
    clearweaponcargo _this;
    clearmagazinecargo _this;
    
    _this addmagazineCargo ["HandGrenade_West", _amountAmmo];
    _this addmagazineCargo ["ACE_C4_M", _amountAmmo];
    _this addmagazineCargo ["ACE_M84", _amountAmmo];
    _this addmagazineCargo ["ACE_CLAYMORE_M", _amountAmmo];
    _this addmagazineCargo ["ACE_M34", _amountAmmo];
    _this addmagazineCargo ["ACE_M2SLAM_M", _amountAmmo];
    _this addmagazineCargo ["ACE_M4SLAM_M", _amountAmmo];
	_this addMagazineCargo ["SmokeShell", _amountAmmo];
	_this addMagazineCargo ["SmokeShellBlue", _amountAmmo];
	_this addMagazineCargo ["SmokeShellGreen", _amountAmmo];
	_this addMagazineCargo ["SmokeShellOrange", _amountAmmo];
	_this addMagazineCargo ["SmokeShellPurple", _amountAmmo];
	_this addMagazineCargo ["SmokeShellRed", _amountAmmo];
	_this addMagazineCargo ["SmokeShellYellow", _amountAmmo];

	
	
    sleep _timer;
}; 


// EOF