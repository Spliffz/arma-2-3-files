//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////
//## rifles filler

//if !(isServer) exitWith{};

_timer = 900;
_amountAmmo = 200;
_amountWeapons = 20;

while {alive _this} do
{
    clearweaponcargo _this;
    clearmagazinecargo _this;

    // DAF
//    _this addWeaponCargo ["daf_ksp90b", _amountWeapons];
    
	_this addWeaponCargo ["RH_hk416", _amountWeapons];
	_this addWeaponCargo ["RH_hk416sd", _amountWeapons];
	_this addWeaponCargo ["RH_hk416gl", _amountWeapons];
	_this addWeaponCargo ["RH_hk416s", _amountWeapons];
	_this addWeaponCargo ["RH_hk416sgl", _amountWeapons];
	_this addWeaponCargo ["RH_hk416sgl", _amountWeapons];
	_this addWeaponCargo ["RH_ctar21", _amountWeapons];
	//_this addWeaponCargo ["RH_hk417", _amountWeapons];
	//_this addWeaponCargo ["RH_hk417s", _amountWeapons];
	//_this addWeaponCargo ["RH_hk417sgl", _amountWeapons];
	_this addWeaponCargo ["RH_acr", _amountWeapons];
	_this addWeaponCargo ["RH_acrb", _amountWeapons];
	_this addWeaponCargo ["RH_acrbgl", _amountWeapons];
	_this addWeaponCargo ["RH_mas", _amountWeapons];
	_this addWeaponCargo ["RH_masb", _amountWeapons];
	_this addWeaponCargo ["RH_m27", _amountWeapons];
	_this addWeaponCargo ["RH_m27c", _amountWeapons];
	_this addWeaponCargo ["DDAM_C7i", _amountWeapons];
	_this addWeaponCargo ["DDAM_C8i", _amountWeapons];
	_this addWeaponCargo ["DDAM_C8SFW_I", _amountWeapons];
	_this addWeaponCargo ["DDAM_C8SFWGL_I", _amountWeapons];
   	_this addWeaponCargo ["DDAM_C7a2i", _amountWeapons];
	_this addWeaponCargo ["DDAM_M95i", _amountWeapons];
	_this addWeaponCargo ["DDAM_M96i", _amountWeapons];
   	_this addWeaponCargo ["DDAM_P210", _amountWeapons];
	_this addWeaponCargo ["M16A2", _amountWeapons];
	_this addWeaponCargo ["M16A2GL", _amountWeapons];
	_this addWeaponCargo ["M16A4_GL", _amountWeapons];
	_this addWeaponCargo ["M4A1", _amountWeapons];
	_this addWeaponCargo ["ACE_M4_GL", _amountWeapons];
	_this addWeaponCargo ["ACE_M4_C", _amountWeapons];
	_this addWeaponCargo ["ACE_M16A4_Iron", _amountWeapons];
	_this addWeaponCargo ["ACE_M4A1_GL", _amountWeapons];
	_this addWeaponCargo ["ACE_SOC_M4A1", _amountWeapons];
	_this addWeaponCargo ["ACE_SOC_M4A1_GL", _amountWeapons];
	_this addWeaponCargo ["ACE_SOC_M4A1_GL_13", _amountWeapons];
	_this addWeaponCargo ["ACE_HK416_D14", _amountWeapons];
    _this addWeaponCargo ["ACE_HK416_D10_M320", _amountWeapons];
	_this addWeaponCargo ["SCAR_L_CQC", _amountWeapons];
	_this addWeaponCargo ["ACE_G36K_iron_D", _amountWeapons];

	//M8 family
//	_this addWeaponCargo ["m8_carbine", _amountWeapons];
//	_this addWeaponCargo ["m8_carbine_pmc", _amountWeapons];
//	_this addWeaponCargo ["m8_carbineGL", _amountWeapons];
//	_this addWeaponCargo ["m8_compact", _amountWeapons];
//	_this addWeaponCargo ["m8_compact_pmc", _amountWeapons];
//	_this addWeaponCargo ["m8_SAW", _amountWeapons];
//	_this addWeaponCargo ["m8_sharpshooter", _amountWeapons];

	_this addWeaponCargo ["ACE_G3A3_RSAS", _amountWeapons];
	_this addWeaponCargo ["ACE_G3A3", _amountWeapons];

	_this addWeaponCargo ["ACE_M27_IAR", _amountWeapons];
	_this addWeaponCargo ["M240", _amountWeapons];
	_this addWeaponCargo ["M249_EP1", _amountWeapons];
	_this addWeaponCargo ["M249", _amountWeapons];
	_this addWeaponCargo ["M1014", _amountWeapons];
	_this addWeaponCargo ["ACE_SPAS12", _amountWeapons];

	_this addMagazineCargo ["8Rnd_B_Beneli_74Slug", _amountAmmo];
	_this addMagazineCargo ["8Rnd_B_Beneli_Pellets", _amountAmmo];
	_this addMagazineCargo ["ACE_8Rnd_12Ga_Buck00", _amountAmmo];
	_this addMagazineCargo ["ACE_8Rnd_12Ga_Slug", _amountAmmo];
	_this addWeaponCargo ["Colt1911", _amountWeapons];
	_this addWeaponCargo ["M9", _amountWeapons];
	_this addWeaponCargo ["glock17_EP1", _amountWeapons];
	_this addWeaponCargo ["A2020_g17", _amountWeapons]; // daf
	_this addWeaponCargo ["ACE_TT", _amountWeapons];
	_this addWeaponCargo ["ACE_USP", _amountWeapons];
	_this addWeaponCargo ["ACE_USPSD", _amountWeapons];
	//_this addWeaponCargo ["revolver_gold_EP1", _amountWeapons];
	_this addMagazineCargo ["17Rnd_9x19_glock17", _amountAmmo];
	_this addMagazineCargo ["ACE_12Rnd_45ACP_USPSD", _amountAmmo];
	_this addMagazineCargo ["6Rnd_45ACP", _amountAmmo];

	_this addWeaponCargo ["ACE_MP5A4", _amountWeapons];
	_this addWeaponCargo ["ACE_MP5A5", _amountWeapons];
	_this addWeaponCargo ["ACE_MP7", _amountWeapons];
	_this addWeaponCargo ["ACE_UMP45", _amountWeapons];
	_this addMagazineCargo ["ACE_15Rnd_9x19_S_M9", _amountAmmo];
	_this addMagazineCargo ["20Rnd_B_765x17_Ball", _amountAmmo];
	_this addMagazineCargo ["30Rnd_9x19_MP5SD", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_9x19_S_MP5", _amountAmmo];
	_this addMagazineCargo ["ACE_40Rnd_B_46x30_MP7", _amountAmmo];
	_this addMagazineCargo ["ACE_FlareIR_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_Smoke_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_SmokeRed_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_SmokeGreen_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_SmokeYellow_M203", _amountAmmo];
	_this addMagazineCargo ["SmokeShell", _amountAmmo];
	_this addMagazineCargo ["SmokeShellBlue", _amountAmmo];
	_this addMagazineCargo ["SmokeShellGreen", _amountAmmo];
	_this addMagazineCargo ["SmokeShellOrange", _amountAmmo];
	_this addMagazineCargo ["SmokeShellPurple", _amountAmmo];
	_this addMagazineCargo ["SmokeShellRed", _amountAmmo];
	_this addMagazineCargo ["SmokeShellYellow", _amountAmmo];
	_this addMagazineCargo ["ACE_100Rnd_556x45_T_M249", _amountAmmo];
	_this addMagazineCargo ["ACE_200Rnd_556x45_T_M249", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_SB_S_Stanag", _amountAmmo];
	_this addMagazineCargo ["ACE_30Rnd_556x45_T_Stanag", _amountAmmo];
	_this addMagazineCargo ["100Rnd_762x51_M240", _amountAmmo];
	_this addMagazineCargo ["100Rnd_556x45_M249", _amountAmmo];
	_this addMagazineCargo ["200Rnd_556x45_M249", _amountAmmo];
	_this addMagazineCargo ["20Rnd_762x51_B_SCAR", _amountAmmo];
	_this addMagazineCargo ["15Rnd_9x19_M9", _amountAmmo];
	_this addMagazineCargo ["7Rnd_45ACP_1911", _amountAmmo];
	_this addMagazineCargo ["HandGrenade_West", _amountAmmo];
	_this addMagazineCargo ["FlareWhite_M203", _amountAmmo];
	_this addMagazineCargo ["FlareYellow_M203", _amountAmmo];
	_this addMagazineCargo ["FlareGreen_M203", _amountAmmo];
	_this addMagazineCargo ["FlareRed_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_Flashbang", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_SB_SCAR", _amountAmmo];
	_this addMagazineCargo ["ACE_1Rnd_HE_M203", _amountAmmo];
	_this addMagazineCargo ["1Rnd_HE_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_1Rnd_CS_M203", _amountAmmo];
	_this addMagazineCargo ["ACE_25Rnd_1143x23_B_UMP45", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_HK417", _amountAmmo];
	_this addMagazineCargo ["ACE_20Rnd_762x51_T_G3", _amountAmmo];
	_this addMagazineCargo ["30Rnd_556x45_G36", _amountAmmo];
	
    sleep _timer;
}; 


// EOF