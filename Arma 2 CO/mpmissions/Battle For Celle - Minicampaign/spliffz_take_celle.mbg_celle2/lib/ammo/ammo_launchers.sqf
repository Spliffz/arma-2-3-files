//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////
//## Launchers filler

//if !(isServer) exitWith{};

_timer = 1800;
_amountAmmo = 100;
_amountWeapons = 20;

while {alive _this} do
{
    clearweaponcargo _this;
    clearmagazinecargo _this;
    
    _this addWeaponCargo ["M136", _amountWeapons];
    _this addWeaponCargo ["ACE_M136_CSRS", _amountWeapons];
    _this addWeaponCargo ["ACE_M72A2", _amountWeapons];

    sleep _timer;
}; 


// EOF