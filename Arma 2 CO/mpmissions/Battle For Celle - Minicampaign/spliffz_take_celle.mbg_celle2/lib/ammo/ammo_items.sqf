//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////
//## Items filler

//if !(isServer) exitWith{};

_timer = 1800;
_amountAmmo = 100;
_amountWeapons = 100; // default 20

while {alive _this} do
{
    clearweaponcargo _this;
    clearmagazinecargo _this;
    
    _this addWeaponCargo ["Binocular", _amountWeapons];
    //_this addWeaponCargo ["Binocular_Vector", _amountWeapons];
    //_this addMagazineCargo ["ACE_Battery_Rangefinder", _amountAmmo];
    _this addWeaponCargo ["ACE_DAGR", _amountWeapons];
    _this addWeaponCargo ["ACE_Earplugs", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesBalaklava", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesBalaklavaGray", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesBalaklavaOlive", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesBlackSun", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesBlueSun", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesGasMask_RU", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesGasMask_US", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesGreenSun", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesLHD_glasses", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesRedSun", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesSunglasses", _amountWeapons];
    _this addWeaponCargo ["ACE_GlassesTactical", _amountWeapons];
    //_this addWeaponCargo ["ACE_HuntIR_monitor", _amountWeapons];
    _this addWeaponCargo ["ACE_Kestrel4500", _amountWeapons];
    _this addWeaponCargo ["ACE_KeyCuffs", _amountWeapons];
    _this addWeaponCargo ["ACE_Map", _amountWeapons];
    _this addWeaponCargo ["ACE_Map_Tools", _amountWeapons];
    _this addWeaponCargo ["ACE_SpareBarrel", _amountWeapons];
    _this addWeaponCargo ["ACE_WireCutter", _amountWeapons];
    _this addWeaponCargo ["ItemCompass", _amountWeapons];
    _this addWeaponCargo ["ItemGPS", _amountWeapons];
    _this addWeaponCargo ["ItemMap", _amountWeapons];
    _this addWeaponCargo ["ItemRadio", _amountWeapons];
    _this addWeaponCargo ["ItemWatch", _amountWeapons];

	_this addMagazineCargo ["ACE_Knicklicht_B", _amountAmmo];
	_this addMagazineCargo ["ACE_Knicklicht_G", _amountAmmo];
	_this addMagazineCargo ["ACE_Knicklicht_IR", _amountAmmo];
	_this addMagazineCargo ["ACE_Knicklicht_R", _amountAmmo];
	_this addMagazineCargo ["ACE_Knicklicht_W", _amountAmmo];
	_this addMagazineCargo ["ACE_Knicklicht_Y", _amountAmmo];

    sleep _timer;
}; 


// EOF