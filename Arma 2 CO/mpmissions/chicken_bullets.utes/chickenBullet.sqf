//////////////////////////////////////////////////
//         A Spliffz Production - 2012
//////////////////////////////////////////////////

_unit = _this select 0;
_weapon = _this select 1;
_muzzle = _this select 2;
_mode = _this select 3;
_ammo = _this select 4;
_magazine = _this select 5;
_projectile = _this select 6;
_velocity = velocity (_this select 6);

hint format ["speed: %1", _velocity];

//_bPos = position _projectile;
//_chicken = createVehicle ["Hen", _bpos, [], 0, "NONE"];


while {alive _projectile} do {
	_dir = getDir _projectile;
	_bPos = position _projectile;
	_chicken setDir _dir;
	_chicken setVelocity velocity+_velocity;
	//deleteVehicle _projectile;
	_chicken attachTo [_projectile,[0,0,0]];
	_chicken setPos _bPos;
};


// EOF