//#######################################
//	init.sqf
//
//  Author: Spliffz <thespliffz@gmail.com>
//#######################################


dyna_blur = ppEffectCreate ["dynamicBlur", -13500];
mor_intox = ppEffectCreate ["wetDistortion", -13504];
chrome_Abb = ppEffectCreate ["ChromaticAberration", -13508];
film_Grain = ppEffectCreate ["filmGrain", -13512];
rad_blur = ppEffectCreate ["radialBlur", -13516];
color_Corr = ppEffectCreate ["colorCorrections", -13520];
color_inv = ppEffectCreate ["colorInversion", -13524];


player addAction ["Particle Fun!", "lib\particles.sqf", ["start"]];
player addAction ["Set ObjectTexture!", "lib\particles.sqf", ["objectTexture"]];
player addAction ["Dyna Fun!", "lib\particles.sqf", ["dyna"]];
player addAction ["Color Inversion Fun!", "lib\particles.sqf", ["colorInv"]];
player addAction ["Film Fun!", "lib\particles.sqf", ["film"]];
player addAction ["Chrome Fun!", "lib\particles.sqf", ["chrome"]];
player addAction ["Morphine Fun!", "lib\particles.sqf", ["morph"]];
player addAction ["Particle Epicness!", "lib\particles.sqf", ["epicness"]];
player addAction ["Stop Particle Fun!", "lib\particles.sqf", ["stop"]];


// EOF