//#######################################
//	particles.sqf
//
//  Author: Spliffz <thespliffz@gmail.com>
//#######################################

_unit = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_params = _this select 3;

spliffz_radialBlur = compile preProcessFileLineNumbers "lib\fnc_rBlur.sqf";
spliffz_paddos = compile preProcessFileLineNumbers "lib\fnc_colorInv.sqf";


switch (_params select 0) do {
	case "objectTexture": {
		player setObjectTexture [0,'#(argb,8,8,3)color(0,1,0,1)'];
	};

	case "morph": {
		mor_intox ppEffectEnable true;
		mor_intox ppEffectAdjust [0, 0.1, 0, 0, 0.25, 0, 0, 0, 0.25, 0, 0, 0, 0, 2, 0];
		mor_intox ppEffectCommit 0;
//		[player,30,0.03] call ace_fx_fnc_dizzy;
		[player, 30, 0.035] call ace_fx_fnc_dizzy;
	};
	
	case "start": {
	
		/*
		eff = ppEffectCreate ["radialBlur", 2005];
		eff ppEffectEnable true;
		eff ppEffectAdjust [0.9,0.9,0,0];
		eff ppEffectCommit 10;
		sleep 5;
		*/

		eff2 = ppEffectCreate ["ColorCorrections", 2000];
		eff2 ppEffectEnable true;
		//eff2 ppEffectAdjust [0.6, 0.7, 0.2, 0.2, 0.2, 0.3, 0.3, 0.4, 0.5, 0.5, 0.6, 0.7, 0.4, 0.6, 0.7];
		eff2 ppEffectAdjust [0, 0.2, 0, 0, 0.34, 0, 0, 0, 0.34, 0, 0, 0, 0, 2.5, 0];
		//eff2 ppEffectCommit 10;
		eff2 ppEffectCommit 10;
		sleep 5;


		/*
		_eff2 = ppEffectCreate ["colorInversion", 2001];
		_eff2 ppEffectEnable true;
		_eff2 ppEffectAdjust [0.6, 0.7, 0.4];
		_eff2 ppEffectCommit 10;
		sleep 5;
		*/
	
	};
	
	case "stop": {
		if(ppEffectCommitted rad_blur) then {
			ppEffectDestroy rad_blur;
		};

		if(ppEffectCommitted color_Corr) then {
			ppEffectDestroy color_Corr;
		};

		if(ppEffectCommitted mor_intox) then {
			ppEffectDestroy mor_intox;
		};

		if(ppEffectCommitted dyna_blur) then {
			ppEffectDestroy dyna_blur;
		};

		if(ppEffectCommitted chrome_Abb) then {
			ppEffectDestroy chrome_Abb;
		};

		if(ppEffectCommitted film_Grain) then {
			ppEffectDestroy film_Grain;
		};
		
		if(ppEffectCommitted color_inv) then {
			ppEffectDestroy color_inv;
		};
	};
	
	case "dyna": {
		dyna_blur ppEffectEnable true;
		dyna_blur ppEffectAdjust [0.1];
		dyna_blur ppEffectCommit 0;
	};
	
	case "chrome": {
		chrome_Abb ppEffectEnable true;
		chrome_Abb ppEffectAdjust [3.1, 3.1, true];
		chrome_Abb ppEffectCommit 10;
	};
	
	case "film": {
		film_Grain ppEffectEnable true;
		film_Grain ppEffectAdjust [0.2, 3, 1, 1, 1, false];
		film_Grain ppEffectCommit 0;
	};
	
	case "colorInv": {
		color_inv ppEffectEnable true;
		color_inv ppEffectAdjust [1, 0, 0];  //RGB
		color_inv ppEffectCommit 0;
	};
	
	case "epicness": {
		// mmmm Morphine
		mor_intox ppEffectEnable true;
		mor_intox ppEffectAdjust [0, 0.1, 0, 0, 0.25, 0, 0, 0, 0.25, 0, 0, 0, 0, 2, 0];
		mor_intox ppEffectCommit 0;
//		[player,30,0.03] call ace_fx_fnc_dizzy;
		[player, 10, 0.035] call ace_fx_fnc_dizzy;
		//_sleep_time = 30.1;
		
		sleep 5;
		
		// mmm LSD
		[player, 10] spawn spliffz_radialBlur;
		
		
		// mmm paddo's
		[player, 10] spawn spliffz_paddos;
		
		sleep 5;
		
		// mmm wtf00k
		color_Corr = ppEffectCreate ["ColorCorrections", 2000];
		color_Corr ppEffectEnable false;
		//color_Corr ppEffectAdjust [0.6, 0.7, 0.2, 0.2, 0.2, 0.3, 0.3, 0.4, 0.5, 0.5, 0.6, 0.7, 0.4, 0.6, 0.7];
		color_Corr ppEffectAdjust [0.6, 0.7, 0.2, 0.2, 0.24, 0.3, 0.34, 0.4, 0.5, 0.5, 0.6, 1.7, 1.1, 0.2, 0];
		//color_Corr ppEffectAdjust [0, 0.2, 0, 0, 0.34, 0, 0, 0, 0.34, 0, 0, 0, 0, 2.5, 0];
		color_Corr ppEffectCommit 10;
		
		
	};

};

// EOF