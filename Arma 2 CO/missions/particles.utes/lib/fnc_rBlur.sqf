//#######################################
//	fnc_rBlur.sqf
//
//  Author: Spliffz <thespliffz@gmail.com>
//#######################################

_player = _this select 0;
_dur = _this select 1;

spliffz_fnc_radialBlur = {
	rad_blur ppEffectEnable false;
	sleep (random 5);
	rad_blur = ppEffectCreate ["radialBlur", 2005];
	rad_blur ppEffectEnable true;
	_blur = 0;
	for [{_i=1},{_i<=10},{_i=_i+1}] do {
		rad_blur ppEffectAdjust [_blur, _blur, 0.1, 0.1];
		rad_blur ppEffectCommit 0;
		_blur = _blur + 0.005;
		sleep 0.5;
	};
};


while{true} do {
	
	call spliffz_fnc_radialBlur;
	
	sleep _dur;
};

sleep _dur;

// EOF