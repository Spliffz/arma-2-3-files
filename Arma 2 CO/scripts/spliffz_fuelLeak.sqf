//----------------------------------------------------------
// Fuel Leak Script
// leaks fuel when vehicle not on road.
// Spliffz <thespliffz@gmail.com>
//
// drive of your life
// fuel leak
//
// put this in the init of the vehicle:
// null = [this, "doyl"] execVM "spliffz_fuelLeak.sqf"; 
//
// params: 
//	- this
// 	- doyl/leak
//----------------------------------------------------------

_vehicle = _this select 0;
_soort = _this select 1;


while {alive _vehicle} do {
	
	if(!isOnRoad _vehicle) then {
		switch(_soort) do {
			case "doyl": {
				_vehicle setDamage 1;
			};
			
			case "leak": {
				_curFuel = fuel _vehicle;
				if(_curFuel > 0) then {
					while(_curFuel > 0) do {
						_curFuel = _curFuel - 0.1;
						sleep 1; // 1 sec correction time
					};
				};
				
			};
		};
	};
	
	sleep 1; 
};


// EOF