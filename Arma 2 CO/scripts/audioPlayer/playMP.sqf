//**************************
// Audio Player script
// v0.11
//
// \Spliffz\audioPlayer\play.sqf
//
// Made by Spliffz[TDNL] <thespliffz@gmail.com>
//**************************

_playMusicArray = _this select 0;
//_playMusicName = _playMusicArray select 0;
//_playMusicVal = _playMusicArray select 1;
_numItem = _this select 1;
_req = _this select 2;

#include "defines.sqf";

//_num = spliffz_audioPlayerItemsArray select _reqAmmoSelect;

//hint "bla - "+_numItem;

_req say3D _numItem;

spliffz_playMusic = 0;


//_req removeAction _id;
{ _req removeAction _x } forEach spliffz_audioPlayerNumberArray;

_dur = spliffz_audioPlayerDuration select _reqAmmoSelect;
sleep _dur;

{ _req addAction[("<t color='"+spliffz_audioPlayerFontcolor+"'>" + (spliffz_audioPlayerTextArray select _forEachIndex) + "</t>"), spliffz_audioPlayerReq_path+"play.sqf", [spliffz_audioPlayerNumberArray select _forEachIndex], 6] } forEach spliffz_audioPlayerItemsArray;

// EOF