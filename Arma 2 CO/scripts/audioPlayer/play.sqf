//**************************
// Audio Player script
// v0.11
//
// \Spliffz\audioPlayer\play.sqf
//
// Made by Spliffz[TDNL] <thespliffz@gmail.com>
//**************************

_req = _this select 0;
spliffz_audioPlayerReq = _req;
_caller = _this select 1;
_id = _this select 2;
_reqAmmoArray = _this select 3;
_reqAmmoSelect = _reqAmmoArray select 0;
publicVariable "spliffz_audioPlayerReq";

#include "defines.sqf";

_num = spliffz_audioPlayerItemsArray select _reqAmmoSelect;
spliffz_audioPlayerNumber = _num;
publicVariable "spliffz_audioPlayerNumber";

spliffz_playMusic = 1;

// new playmusic variable value sent to other non-local machines, to trigger eventhandler
publicVariable "spliffz_playMusic"; 

//for client (activator) to play script.
[spliffz_playMusic, spliffz_audioPlayerNumber, spliffz_audioPlayerReq] execVM spliffz_audioPlayerReq_path+"playMP.sqf"; 

// EOF