//**************************
// Audio Player script
// v0.11
//
// \Spliffz\audioPlayer\audioPlayer.sqf
//
// Made by Spliffz[TDNL] <thespliffz@gmail.com>
//**************************
    
#include "defines.sqf";

_objct = _this select 0;
spliffz_audioPlayerobjct = _this select 0;

spliffz_playMusic = 0;
"spliffz_playMusic" addPublicVariableEventHandler 
{ 
        [_this select 1, spliffz_audioPlayerNumber, spliffz_audioPlayerReq] execVM spliffz_audioPlayerReq_path+"playMP.sqf"; 
    //[_this select 1, spliffz_audioPlayerNumber, _objct] execVM spliffz_audioPlayerReq_path+"playMP.sqf"; 
}; 

spliffz_audioPlayerobjct allowDamage false;

{ _objct addAction[("<t color='"+spliffz_audioPlayerFontcolor+"'>" + (spliffz_audioPlayerTextArray select _forEachIndex) + "</t>"), spliffz_audioPlayerReq_path+"play.sqf", [spliffz_audioPlayerNumberArray select _forEachIndex], 6] } forEach spliffz_audioPlayerItemsArray;

// EOF