//**************************
// Audio Player script
// v0.11
//
// \Spliffz\audioPlayer\defines.sqf
//
// Made by Spliffz[TDNL] <thespliffz@gmail.com>
//**************************

// Configuration
spliffz_audioPlayerfontcolor = "#E8CD46"; // set font color in scroll menu

// DO NOT EDIT BELOW
spliffz_audioPlayerReq_path = "Spliffz\audioPlayer\";

spliffz_audioPlayerItemsArray = ["sound_FortunateSon", "sound_AmericaFuckYeah"];
spliffz_audioPlayerNumberArray = [0,1];
spliffz_audioPlayerTextArray = ["Play Fortunate Son", "Play America Fuck Yeah!"];
spliffz_audioPlayerDuration = [140, 140];

//spliffz_audioPlayerNumber = "";
//spliffz_audioPlayerReq = "";

publicVariable "spliffz_audioPlayerItemsArray";
publicVariable "spliffz_audioPlayerNumberArray";
publicVariable "spliffz_audioPlayerTextArray";
publicVariable "spliffz_audioPlayerDuration";

// EOF