//**************************
// Request stuff script
// v0.12
//
// Made by Spliffz[TDNL] <thespliffz@gmail.com>
//
//**************************

[Information]
So there I was, flying around, doing recon for my squad on the ground when I got a resupply call.
I figured why not, so I go RTB and start loading up my Bird.
So while I'm walking back and fourth from the ammocrate to the bird I came up with this.
I call it the Request Stuff script.
It basically lets you request stuff like supplies or empty crates, so you can fill it yourself and THEN put it in the chopper.. yes I know.. simply genius.
After that you need something like R3F Logistics or something to put in in a vehicle, but that's your problem ;)
I originally made this for our Patrol Ops which allready has R3F in it.

[Usage]
put this in unit initfield:
    null = [this] execVM "REQUEST\init.sqf";

then put a marker on the map named
    mrk_req_spawn

then walk up to that unit and look in your scroll menu
items you request will spawn on the marker.

I included a test mission.


[Can be requested]
US Army Ammunition Crate
US Army Ordnance Crate
Small empty crate
medium empty crate
large empty crate
Medical crate (ACE)
