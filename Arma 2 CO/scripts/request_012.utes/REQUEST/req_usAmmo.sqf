//**************************
// Request stuff script
// v0.12
//
// \REQUEST\req_usAmmo.sqf
//
// Made by Spliffz[TDNL] <thespliffz@gmail.com>
//**************************

_req = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_reqAmmoArray = _this select 3;
_reqAmmoSelect = _reqAmmoArray select 0;

#include "defines.sqf";

req_crate = createVehicle [itemsArray select _reqAmmoSelect, getMarkerPos "mrk_req_spawn", [], 0, "NONE"];
if(_reqAmmoSelect in emptyCrates) then {
    clearWeaponCargo req_crate;
    clearMagazineCargo req_crate;
};

// EOF