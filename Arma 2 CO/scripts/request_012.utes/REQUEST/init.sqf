//**************************
// Request stuff script
// v0.12
//
// Made by Spliffz[TDNL] <thespliffz@gmail.com>
//
// \REQUEST\init.sqf
//
// Usage:
// put this in unit initfield:
//   null = [this] execVM "REQUEST\init.sqf";
//
// then put a marker on the map named
//   mrk_req_spawn
//
// then walk up to that unit and look in your scroll menu
// items you request will spawn on the marker.
//
//**************************
#include "defines.sqf";

// Configuration
fontcolor = "#E8CD46"; // set font color in scroll menu

// DO NOT EDIT BELOW!!!
req_path = "REQUEST\";

regMan = _this select 0;
_reqMan = _this select 0;

regMan allowDamage false;
doStop regMan;
removeAllWeapons regMan;
removeAllItems regMan;

{ _reqMan addAction[("<t color='"+fontcolor+"'>" + (textArray select _forEachIndex) + "</t>"), req_path+"req_usAmmo.sqf", [numberArray select _forEachIndex], 6] } forEach itemsArray;

// EOF