//**************************
// Request stuff script
// v0.12
//
// \REQUEST\defines.sqf
//
// Made by Spliffz[TDNL] <thespliffz@gmail.com>
//**************************


itemsArray = ["USBasicAmmunitionBox_EP1", "USOrdnanceBox_EP1", "USBasicAmmunitionBox_EP1", "USBasicWeapons_EP1", "USVehicleBox_EP1", "ACE_BandageBoxWest"];
//numberArray = [1,2,3,4,5,6];
numberArray = [0,1,2,3,4,5];
textArray = ["Request US Army Ammo Crate", "Request US Ordnance Crate", "Request Empty Crate - small", "Request Empty Crate - medium", "Request Empty Crate - large", "Medical Supply Box"];
emptyCrates = [2,3,4];

publicVariable "itemsArray";
publicVariable "textArray";
publicVariable "numberArray";
publicVariable "emptyCrates";

// EOF